%define gstreamername gstreamer1
%global __python %{__python3}

Name: @PACKAGE_NAME@
Version: @PACKAGE_VERSION@
Release: 1%{?dist}
Summary: GSTLAL Calibration
License: GPL
Group: LSC Software/Data Analysis

Requires: gstlal >= @MIN_GSTLAL_VERSION@
Requires: gstlal-ugly >= @MIN_GSTLALUGLY_VERSION@
Requires: python3 >= @MIN_PYTHON_VERSION@
Requires: %{gstreamername} >= @MIN_GSTREAMER_VERSION@
Requires: %{gstreamername}-plugins-base >= @MIN_GSTREAMER_VERSION@
Requires: %{gstreamername}-plugins-good >= @MIN_GSTREAMER_VERSION@
Requires: numpy
Requires: scipy
Requires: lal >= @MIN_LAL_VERSION@
Requires: lalmetaio >= @MIN_LALMETAIO_VERSION@
Requires: python%{python3_pkgversion}-%{gstreamername}
Requires: python%{python3_pkgversion}-ligo-segments >= @MIN_LIGO_SEGMENTS_VERSION@

BuildRequires: pkgconfig >= @MIN_PKG_CONFIG_VERSION@
BuildRequires: gstlal-devel >= @MIN_GSTLAL_VERSION@
BuildRequires: python3-devel >= @MIN_PYTHON_VERSION@
BuildRequires: fftw-devel >= 3
BuildRequires: %{gstreamername}-devel >= @MIN_GSTREAMER_VERSION@
BuildRequires: %{gstreamername}-plugins-base-devel >= @MIN_GSTREAMER_VERSION@
BuildRequires: lal-devel >= @MIN_LAL_VERSION@
BuildRequires: lalmetaio-devel >= @MIN_LALMETAIO_VERSION@

Conflicts: gstlal-ugly < 0.6.0
Source: https://software.igwn.org/lscsoft/source/@PACKAGE_NAME@-%{version}.tar.gz
URL: https://git.ligo.org/lscsoft/gstlal
Packager: Madeline Wade <madeline.wade@gravity.phys.uwm.edu>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package contains the plugins and shared libraries required to run the gstlal calibration software.


%package devel
Summary: Files and documentation needed for compiling gstlal-calibration based plugins and programs.
Group: LSC Software/Data Analysis
Requires: gstlal-devel >= @MIN_GSTLAL_VERSION@
Requires: python3-devel >= @MIN_PYTHON_VERSION@
Requires: %{gstreamername}-devel >= @MIN_GSTREAMER_VERSION@
Requires: %{gstreamername}-plugins-base-devel >= @MIN_GSTREAMER_VERSION@
Requires: lal-devel >= @MIN_LAL_VERSION@
Requires: lalmetaio-devel >= @MIN_LALMETAIO_VERSION@
Requires: gsl-devel
%description devel
This package contains the files needed for building gstlal-calibration based
plugins and programs.


%prep
%setup -q -n %{name}-%{version}


%build
%configure PYTHON=python3
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gstlal/*
%{_libdir}/gstreamer-*/lib*.a
%{_libdir}/gstreamer-*/lib*.so
%{_prefix}/%{_lib}/python*/site-packages/gstlal
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/gstreamer-*/lib*.a
%{_libdir}/pkgconfig/*
%{_includedir}/*
