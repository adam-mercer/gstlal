#!/usr/bin/env python3
# Copyright (C) 2021  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import numpy as np
from optparse import OptionParser, Option


parser = OptionParser()
parser.add_option("--fs-squared-txt", metavar = "name", help = "Name of txt file with a time series of fs^2 values")
parser.add_option("--Qinv-txt", metavar = "name", help = "Name of txt file with a time series of 1/Q values")
parser.add_option("--filename", metavar = "name", type = str, default = "fs_over_Q.txt", help = "Name of output file")

options, filenames = parser.parse_args()


fs2 = np.loadtxt(options.fs_squared_txt)
Qinv = np.loadtxt(options.Qinv_txt)
t = np.transpose(fs2)[0]
fs2 = np.transpose(fs2)[1]
Qinv = np.transpose(Qinv)[1]

fs_over_Q = np.zeros(len(fs2))

for i in range(len(fs2)):
	fs_over_Q[i] = np.sign(fs2[i]) * np.sqrt(abs(fs2[i])) * Qinv[i]

np.savetxt(options.filename, np.transpose([t, fs_over_Q]), fmt = ['%f', '%8e'], delimiter = "   ")


