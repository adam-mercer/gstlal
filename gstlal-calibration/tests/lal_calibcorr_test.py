#!/usr/bin/env python3
# Copyright (C) 2022  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import numpy as np
import sys
from gstlal import pipeparts
from gstlal import calibration_parts
import test_common
from gi.repository import Gst


#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


#
# A function to add lines to d_err data.  Simulates the injection channels with
# lines at the given frequencies with the given amplitudes.  Transfer functions
# at those frequencies are used to compute the lines in the real data.  A
# provided DQ vector indicates when the lines are on.
#

def add_lines(pipeline, data, freqs, inj_rate, amplitude, tfs, dq, barf = False):

	asd = np.transpose(np.loadtxt("asd_derr"))[1]
	amplitudes = []
	for i in range(len(freqs)):
		amplitudes.append(asd[int(round(4 * freqs[i]))] * amplitude / abs(tfs[i]))
		if barf:
			print("|exc[%f]| = %e" % (freqs[i], amplitudes[i]))
			print("|derr[%f]| = %e" % (freqs[i], amplitudes[i] * abs(tfs[i])))

	dq = pipeparts.mkgeneric(pipeline, dq, "lal_typecast")
	dq = pipeparts.mktee(pipeline, pipeparts.mkcapsfilter(pipeline, dq, "audio/x-raw,format=F64LE"))
	inj_dq = pipeparts.mktee(pipeline, calibration_parts.mkresample(pipeline, dq, 0, False, inj_rate))
	data_dq = pipeparts.mktee(pipeline, calibration_parts.mkresample(pipeline, dq, 0, False, 16384))
	inj_lines = []
	data_lines = [data]
	for i in range(len(freqs)):
		inj_lines.append(pipeparts.mkgeneric(pipeline, pipeparts.mkgeneric(pipeline, inj_dq, "lal_demodulate", line_frequency = -freqs[i], prefactor_real = amplitudes[i], prefactor_imag = 0), "creal"))
		data_lines.append(pipeparts.mkgeneric(pipeline, pipeparts.mkgeneric(pipeline, data_dq, "lal_demodulate", line_frequency = -freqs[i], prefactor_real = amplitudes[i] * np.real(tfs[i]), prefactor_imag = amplitudes[i] * np.imag(tfs[i])), "creal"))
	inj = calibration_parts.mkadder(pipeline, inj_lines)
	data = calibration_parts.mkadder(pipeline, data_lines)

	return inj, data


#
# A function to simulate frequency-dependent error
#

def simulate_error(df, fmax, amplitude):
	n = int(round(fmax / df)) + 1
	x = np.zeros(n)
	x[0] = np.random.rand() - 0.5
	i = 1
	log = -1
	while i < n:
		newlog = int(np.log(i) / np.log(8))
		if newlog > log:
			x[i] = np.random.rand() - 0.5
			log = newlog
		else:
			x[i] = x[i - 1]
		i += 1
	mag = np.ones(n)
	for i in range(n):
		win = np.hanning(i+1)
		if i == 1:
			win[0] = 1
			win[1] = 1
		win /= sum(win)
		mag[i] += 2 * amplitude * sum(win * x[:i+1])

	x = np.zeros(n)
	x[0] = np.random.rand() - 0.5
	i = 1
	log = -1
	while i < n:
		newlog = int(np.log(i) / np.log(8))
		if newlog > log:
			x[i] = np.random.rand() - 0.5
			log = newlog
		else:
			x[i] = x[i - 1]
		i += 1
	phase = np.zeros(n)
	for i in range(n):
		win = np.hanning(i+1)
		if i == 1:
			win[0] = 1
			win[1] = 1
		win /= sum(win)
		phase[i] = 2 * amplitude * sum(win * x[:i+1])

	return mag * np.exp(1j * phase)


#
# Read real data from raw frames and calibrate it with real GDS calibration filters.
# No TDCFs are computed or applied for simplicity.  Simulate small deviations from
# the models of C, A_TST, A_PUM, and A_UIM.  Apply these to simulated comb injections
# From Pcal, x_TST, x_PUM, and x_UIM to compute and add their effect into d_err. Use
# lal_calibcorr to compute and compensate for this error and test whether it works.
#

def lal_calibcorr_01(pipeline, name):

	# Get the filters and stuff we need from them.
	filters = np.load("/home/aaron.viets/src/gstlal/gstlal-calibration/tests/check_calibration/gds-dcs-filter-generation/filters/PreER15/GDSFilters/H1GDS_1324762762.npz")
	if "res_highpass" in filters:
		invsens_highpass = filters["res_highpass"]
		invsens_highpass_delay = int(filters['res_highpass_delay'])
		invsens_highpass_sr = int(filters['res_highpass_sr']) if 'res_highpass_sr' in filters else hoft_sr
	elif "inv_sensing_highpass" in filters:
		invsens_highpass = filters["inv_sensing_highpass"]
		invsens_highpass_delay = int(filters['invsens_highpass_delay'])
		invsens_highpass_sr = int(filters['invsens_highpass_sr']) if 'invsens_highpass_sr' in filters else hoft_sr
	else:
		invsens_highpass = []
	reschaindelay = int(filters["res_corr_delay"])
	reschainfilt = filters["res_corr_filter"]
	hoft_sr = 16384
	tstdelay = pumdelay = uimdelay = int(filters["ctrl_corr_delay"])
	tstfilt = filters["TST_corr_filter"] if "TST_corr_filter" in filters else filters["ctrl_corr_filter"]
	pumfilt = filters["PUM_corr_filter"] if "PUM_corr_filter" in filters else filters["ctrl_corr_filter"]
	uimfilt = filters["UIM_corr_filter"] if "UIM_corr_filter" in filters else filters["ctrl_corr_filter"]
	tstchainsr = pumchainsr = uimchainsr = int(filters["ctrl_corr_sr"])

	channel_list = []
	channel_list.append(('H1', 'CAL-DELTAL_CTRL_TST_DBL_DQ'))
	channel_list.append(('H1', 'CAL-DELTAL_CTRL_PUM_DBL_DQ'))
	channel_list.append(('H1', 'CAL-DELTAL_CTRL_UIM_DBL_DQ'))
	channel_list.append(('H1', 'CAL-DELTAL_RESIDUAL_DBL_DQ'))
	channel_list.append(('H1', 'CAL-DARM_ERR_DBL_DQ'))
	channel_list.append(('H1', 'CAL-DARM_CTRL_DBL_DQ'))

	# Models of calibration components
	cres_calibcorr = filters['cres_calibcorr']
	all_freqs = cres_calibcorr[0]
	cres_calibcorr = cres_calibcorr[1] + 1j * cres_calibcorr[2]
	D_calibcorr = filters['D_calibcorr']
	D_calibcorr = D_calibcorr[1] + 1j * D_calibcorr[2]
	tst_calibcorr = filters['tst_calibcorr']
	tst_calibcorr = tst_calibcorr[1] + 1j * tst_calibcorr[2]
	Ftst_calibcorr = filters['Ftst_calibcorr']
	Ftst_calibcorr = Ftst_calibcorr[1] + 1j * Ftst_calibcorr[2]
	pum_calibcorr = filters['pum_calibcorr']
	pum_calibcorr = pum_calibcorr[1] + 1j * pum_calibcorr[2]
	Fpum_calibcorr = filters['Fpum_calibcorr']
	Fpum_calibcorr = Fpum_calibcorr[1] + 1j * Fpum_calibcorr[2]
	uim_calibcorr = filters['uim_calibcorr']
	uim_calibcorr = uim_calibcorr[1] + 1j * uim_calibcorr[2]
	Fuim_calibcorr = filters['Fuim_calibcorr']
	Fuim_calibcorr = Fuim_calibcorr[1] + 1j * Fuim_calibcorr[2]

	pcal_corr_calibcorr = filters['pcal_corr_calibcorr']
	daqdownsampling_tst_calibcorr = filters['daqdownsampling_tst_calibcorr']
	daqdownsampling_pum_calibcorr = filters['daqdownsampling_pum_calibcorr']
	daqdownsampling_uim_calibcorr = filters['daqdownsampling_uim_calibcorr']

	pcal_freqs = pcal_corr_calibcorr[0]
	tst_freqs = daqdownsampling_tst_calibcorr[0]
	pum_freqs = daqdownsampling_pum_calibcorr[0]
	uim_freqs = daqdownsampling_uim_calibcorr[0]
	pcal_corr_calibcorr = pcal_corr_calibcorr[1] + 1j * pcal_corr_calibcorr[2]
	daqdownsampling_tst_calibcorr = daqdownsampling_tst_calibcorr[1] + 1j * daqdownsampling_tst_calibcorr[2]
	daqdownsampling_pum_calibcorr = daqdownsampling_pum_calibcorr[1] + 1j * daqdownsampling_pum_calibcorr[2]
	daqdownsampling_uim_calibcorr = daqdownsampling_uim_calibcorr[1] + 1j * daqdownsampling_uim_calibcorr[2]
	pcal_indices = []
	tst_indices = []
	pum_indices = []
	uim_indices = []
	for i in range(len(all_freqs)):
		if all_freqs[i] in pcal_freqs:
			pcal_indices.append(i)
		elif all_freqs[i] in tst_freqs:
			tst_indices.append(i)
		elif all_freqs[i] in pum_freqs:
			pum_indices.append(i)
		elif all_freqs[i] in uim_freqs:
			uim_indices.append(i)
	pcal_indices = np.asarray(pcal_indices)
	tst_indices = np.asarray(tst_indices)
	pum_indices = np.asarray(pum_indices)
	uim_indices = np.asarray(uim_indices)

	# Errors in the models
	C_error = np.ones(16384) #simulate_error(1, 16384, 0.02)
	tst_error = np.ones(16384) #simulate_error(1, 16384, 0.02)
	pum_error = np.ones(16384) #simulate_error(1, 16384, 0.02)
	uim_error = np.ones(16384) #simulate_error(1, 16384, 0.02)

	C_error_calibcorr = []
	tst_error_calibcorr = []
	pum_error_calibcorr = []
	uim_error_calibcorr = []
	for f in all_freqs:
		w1 = f - int(f)
		w0 = 1.0 - w1
		C_error_calibcorr.append(w0 * C_error[int(f)] + w1 * C_error[1 + int(f)])
		tst_error_calibcorr.append(w0 * tst_error[int(f)] + w1 * tst_error[1 + int(f)])
		pum_error_calibcorr.append(w0 * pum_error[int(f)] + w1 * pum_error[1 + int(f)])
		uim_error_calibcorr.append(w0 * uim_error[int(f)] + w1 * uim_error[1 + int(f)])
	C_error_calibcorr = np.asarray(C_error_calibcorr)
	tst_error_calibcorr = np.asarray(tst_error_calibcorr)
	pum_error_calibcorr = np.asarray(pum_error_calibcorr)
	uim_error_calibcorr = np.asarray(uim_error_calibcorr)

	# Apply the errors
	cres_calibcorr *= C_error_calibcorr
	tst_calibcorr *= tst_error_calibcorr
	pum_calibcorr *= pum_error_calibcorr
	uim_calibcorr *= uim_error_calibcorr

	# Find the response function R
	fcc = float(filters['fcc'])
	fs_squared = float(filters['fs_squared'])
	srcQ = float(filters['srcQ'])
	if float(filters['fs_squared']) < 0:
		srcQ = -1j * srcQ
	fs_over_Q = np.real(np.sqrt(complex(fs_squared)) / srcQ)

	R_calibcorr = (1.0 + 1j * all_freqs / fcc) / cres_calibcorr / all_freqs / all_freqs * (all_freqs * all_freqs + fs_squared - 1j * all_freqs * fs_over_Q) + D_calibcorr * (tst_calibcorr * Ftst_calibcorr + pum_calibcorr * Fpum_calibcorr + uim_calibcorr * Fuim_calibcorr)

	pcal_amplitudes = 1000
	tst_amplitudes = 1000
	pum_amplitudes = 1000
	uim_amplitudes = 1000
	pcal_tfs = pcal_corr_calibcorr / R_calibcorr[pcal_indices]
	tst_tfs = tst_calibcorr[tst_indices] / R_calibcorr[tst_indices] / daqdownsampling_tst_calibcorr
	pum_tfs = pum_calibcorr[pum_indices] / R_calibcorr[pum_indices] / daqdownsampling_pum_calibcorr
	uim_tfs = uim_calibcorr[uim_indices] / R_calibcorr[uim_indices] / daqdownsampling_uim_calibcorr

	data = pipeparts.mklalcachesrc(pipeline, location = "H1_easy_raw_frames.cache", cache_dsc_regex = 'H1', use_mmap = True)
	data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = False, channel_list = list(map("%s:%s".__mod__, channel_list)))

	derr = calibration_parts.hook_up(pipeline, data, 'CAL-DARM_ERR_DBL_DQ', 'H1', 1.0)
	derr = pipeparts.mktee(pipeline, calibration_parts.caps_and_progress(pipeline, derr, "audio/x-raw,format=F64LE,rate=16384", "darm_err"))
	dctrl = calibration_parts.hook_up(pipeline, data, 'CAL-DARM_CTRL_DBL_DQ', 'H1', 1.0)
	dctrl = pipeparts.mktee(pipeline, calibration_parts.caps_and_progress(pipeline, dctrl, "audio/x-raw,format=F64LE,rate=16384", "darm_ctrl"))
	pipeparts.mkfakesink(pipeline, dctrl) # In case we don't use it
	res = calibration_parts.hook_up(pipeline, data, 'CAL-DELTAL_RESIDUAL_DBL_DQ', 'H1', 1.0)
	res = calibration_parts.caps_and_progress(pipeline, res, "audio/x-raw,format=F64LE,rate=16384", "res")
	tst = calibration_parts.hook_up(pipeline, data, 'CAL-DELTAL_CTRL_TST_DBL_DQ', 'H1', 1.0)
	tst = calibration_parts.caps_and_progress(pipeline, tst, "audio/x-raw,format=F64LE,rate=4096", "tst")
	tst = calibration_parts.mkresample(pipeline, tst, 5, False, 2048)
	pum = calibration_parts.hook_up(pipeline, data, 'CAL-DELTAL_CTRL_PUM_DBL_DQ', 'H1', 1.0)
	pum = calibration_parts.caps_and_progress(pipeline, pum, "audio/x-raw,format=F64LE,rate=4096", "pum")
	pum = calibration_parts.mkresample(pipeline, pum, 5, False, 2048)
	uim = calibration_parts.hook_up(pipeline, data, 'CAL-DELTAL_CTRL_UIM_DBL_DQ', 'H1', 1.0)
	uim = calibration_parts.caps_and_progress(pipeline, uim, "audio/x-raw,format=F64LE,rate=4096", "uim")
	uim = calibration_parts.mkresample(pipeline, uim, 5, False, 2048)

	# Apply static calibration filters
	tst = pipeparts.mkfirbank(pipeline, tst, latency = tstdelay, fir_matrix = [tstfilt[::-1]], time_domain = True)
	pum = pipeparts.mkfirbank(pipeline, pum, latency = pumdelay, fir_matrix = [pumfilt[::-1]], time_domain = True)
	uim = pipeparts.mkfirbank(pipeline, uim, latency = uimdelay, fir_matrix = [uimfilt[::-1]], time_domain = True)
	# Apply a high-pass filter to the residual path separately.
	if any(invsens_highpass):
		if invsens_highpass_sr != hoft_sr:
			# Magic trick to apply a high-pass filter to the inverse sensing path at a lower sample rate without losing information above the Nyquist frequency.
			res = pipeparts.mktee(pipeline, res)
			res_lowfreq = calibration_parts.mkresample(pipeline, res, 4, False, invsens_highpass_sr, frequency_resolution = (invsens_highpass_sr / 2.0 - 10.0) / 2)
			# Use spectral inversion to make a low-pass filter with a gain of -1.
			invsens_highpass[invsens_highpass_delay] = invsens_highpass[invsens_highpass_delay] - 1.0
			# Apply this filter to the inverse sensing path at a lower sample rate to get only the low frequency components
			res_lowfreq = pipeparts.mkfirbank(pipeline, res_lowfreq, latency = invsens_highpass_delay, fir_matrix = [invsens_highpass[::-1]], time_domain = True)
			# Upsample
			res_lowfreq = calibration_parts.mkresample(pipeline, res_lowfreq, 4, False, hoft_sr, frequency_resolution = (invsens_highpass_sr / 2.0 - 10.0) / 2)
			# Add to the inverse sensing path to get rid of the low frequencies
			res = calibration_parts.mkadder(pipeline, calibration_parts.list_srcs(pipeline, res, res_lowfreq))
	res = pipeparts.mkfirbank(pipeline, res, latency = reschaindelay, fir_matrix = [reschainfilt[::-1]], time_domain = True)
	res = pipeparts.mktee(pipeline, res)
	tst = pipeparts.mktee(pipeline, tst)
	pum = pipeparts.mktee(pipeline, pum)
	uim = pipeparts.mktee(pipeline, uim)

	tst_up = calibration_parts.mkresample(pipeline, tst, 4, False, hoft_sr)
	pum_up = calibration_parts.mkresample(pipeline, pum, 4, False, hoft_sr)
	uim_up = calibration_parts.mkresample(pipeline, uim, 4, False, hoft_sr)

	deltal_model = calibration_parts.mkadder(pipeline, [res, tst_up, pum_up, uim_up])

	# Simulate injection channels with fake lines and add these into d_err.
	dq = calibration_parts.mkpow(pipeline, derr, exponent = 0)
	dq = pipeparts.mkgeneric(pipeline, dq, "lal_demodulate", line_frequency = -0.0002)
	dq = pipeparts.mkgeneric(pipeline, dq, "creal")
	dq = pipeparts.mkgeneric(pipeline, dq, "lal_add_constant", value = 0.2)
	dq = calibration_parts.mkresample(pipeline, dq, 0, False, 16)
	dq = pipeparts.mkgeneric(pipeline, dq, "lal_typecast")
	dq = pipeparts.mkcapsfilter(pipeline, dq, "audio/x-raw,format=U32LE")
	dq = pipeparts.mktee(pipeline, dq)

	pcal, derr = add_lines(pipeline, derr, pcal_freqs, 16384, pcal_amplitudes, pcal_tfs, dq, barf = True)
	tstexc, derr = add_lines(pipeline, derr, tst_freqs, 2048, tst_amplitudes, tst_tfs, dq, barf = True)
	pumexc, derr = add_lines(pipeline, derr, pum_freqs, 1024, pum_amplitudes, pum_tfs, dq, barf = True)
	uimexc, derr = add_lines(pipeline, derr, uim_freqs, 512, uim_amplitudes, uim_tfs, dq, barf = True)
	derr = pipeparts.mktee(pipeline, derr)

	res_calibcorr, tst_calibcorr, pum_calibcorr, uim_calibcorr = calibration_parts.remove_systematic_error(pipeline, res, 16384, tst, tstchainsr, pum, pumchainsr, uim, uimchainsr, derr, pcal, 16384, dq, tstexc, 2048, dq, pumexc, 1024, dq, uimexc, 512, dq, 16, None, None, None, None, None, None, None, None, None, None, filters, 0.125, filename = "calibcorr_filters.txt")

	tst_up = calibration_parts.mkresample(pipeline, tst_calibcorr, 4, False, hoft_sr)
	pum_up = calibration_parts.mkresample(pipeline, pum_calibcorr, 4, False, hoft_sr)
	uim_up = calibration_parts.mkresample(pipeline, uim_calibcorr, 4, False, hoft_sr)

	deltal_calibcorr = calibration_parts.mkadder(pipeline, [res_calibcorr, tst_up, pum_up, uim_up])

	deltal_model = calibration_parts.mkinsertgap(pipeline, deltal_model, chop_length = 1000000000 * 1000)
	deltal_model = pipeparts.mktee(pipeline, deltal_model)
	deltal_calibcorr = calibration_parts.mkinsertgap(pipeline, deltal_calibcorr, chop_length = 1000000000 * 1000)
	tf = calibration_parts.mkinterleave(pipeline, [deltal_model, deltal_calibcorr])
	tf = pipeparts.mkprogressreport(pipeline, tf, "sink")
	calibration_parts.mktransferfunction(pipeline, tf, fft_length = 16 * 16384, fft_overlap = 8 * 16384, num_ffts = 200, fft_window_type = 3, frequency_resolution = 1, filename = "calibcorr_tf.txt")

	pipeparts.mkgeneric(pipeline, derr, "lal_asd", fft_samples = 16384 * 64, overlap_samples = 48, window_type = 3)

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


test_common.build_and_run(lal_calibcorr_01, "lal_calibcorr_01")


