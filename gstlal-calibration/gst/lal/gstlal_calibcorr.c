/*
 * Copyright (C) 2021  Aaron Viets <aaron.viets@ligo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


/*
 * ========================================================================
 *
 *				  Preamble
 *
 * ========================================================================
 */


/*
 * stuff from the C library
 */


#include <complex.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


/*
 * stuff from glib/gstreamer
 */


#include <glib.h>
#include <glib/gprintf.h>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <gst/audio/audio.h>
#include <gst/audio/audio-format.h>


/*
 * our own stuff
 */


#include <gstlal/gstlal.h>
#include <gstlal/gstlal_audio_info.h>
#include <gstlal/gstlal_debug.h>
#include <gstlal_firtools.h>
#include <gstlal_calibcorr.h>


/*
 * ============================================================================
 *
 *			   GStreamer Boilerplate
 *
 * ============================================================================
 */


#define GST_CAT_DEFAULT gstlal_calibcorr_debug
GST_DEBUG_CATEGORY_STATIC(GST_CAT_DEFAULT);


G_DEFINE_TYPE_WITH_CODE(
	GSTLALCalibCorr,
	gstlal_calibcorr,
	GST_TYPE_BASE_SINK,
	GST_DEBUG_CATEGORY_INIT(GST_CAT_DEFAULT, "lal_calibcorr", 0, "lal_calibcorr element")
);


enum property {
	ARG_CINV_FREQS = 1,
	ARG_CINV_SR,
	ARG_CINV_FIR_LENGTH,
	ARG_CRES_MODEL,
	ARG_MEASURED_RESPONSE,
	ARG_CINVCORR_TF,
	ARG_CINVCORR_FILT,
	ARG_D,
	ARG_TST_FREQS,
	ARG_TST_SR,
	ARG_TST_FIR_LENGTH,
	ARG_FTST_MODEL,
	ARG_TST_MODEL,
	ARG_TST_TF,
	ARG_TSTCORR_TF,
	ARG_TSTCORR_FILT,
	ARG_PUM_FREQS,
	ARG_PUM_SR,
	ARG_PUM_FIR_LENGTH,
	ARG_FPUM_MODEL,
	ARG_PUM_MODEL,
	ARG_PUM_TF,
	ARG_PUMCORR_TF,
	ARG_PUMCORR_FILT,
	ARG_UIM_FREQS,
	ARG_UIM_SR,
	ARG_UIM_FIR_LENGTH,
	ARG_FUIM_MODEL,
	ARG_UIM_MODEL,
	ARG_UIM_TF,
	ARG_UIMCORR_TF,
	ARG_UIMCORR_FILT,
	ARG_FILENAME,
	ARG_FAKE
};


static GParamSpec *properties[ARG_FAKE];


/*
 * ============================================================================
 *
 *				  Utilities
 *
 * ============================================================================
 */


static void rebuild_workspace_and_reset(GObject *object) {

	return;
}


static void write_filters(GSTLALCalibCorr *element) {
	int i;
	char *element_name = gst_element_get_name(element);
	FILE *fp;
	fp = fopen(element->filename, "a");
	g_fprintf(fp, "Transfer functions and FIR filters computed by %s\nGPS time of completion: %f\n", element_name, (double) element->pts / GST_SECOND);
	g_fprintf(fp, "\nInverse Sensing Correction TF\nFrequency (Hz)\t\tReal\t\t\tImag\n");
	for(i = 0; i < element->cinvcorr_tf_length; i++)
		g_fprintf(fp, "%f\t%1.15e\t%1.15e\n", (double) i * element->cinv_sr / 2 / (element->cinvcorr_tf_length - 1), creal(element->cinvcorr_tf[i]), cimag(element->cinvcorr_tf[i]));
	g_fprintf(fp, "\nInverse Sensing Correction FIR Filter\n");
	for(i = 0; i < element->cinv_fir_length; i++)
		g_fprintf(fp, "%1.15e\n", element->cinvcorr_filt[i]);
	g_fprintf(fp, "\nA_tst Correction TF\nFrequency (Hz)\t\tReal\t\t\tImag\n");
	for(i = 0; i < element->tstcorr_tf_length; i++)
		g_fprintf(fp, "%f\t%1.15e\t%1.15e\n", (double) i * element->tst_sr / 2 / (element->tstcorr_tf_length - 1), creal(element->tstcorr_tf[i]), cimag(element->tstcorr_tf[i]));
	g_fprintf(fp, "\nA_tst Correction FIR Filter\n");
	for(i = 0; i < element->tst_fir_length; i++)
		g_fprintf(fp, "%1.15e\n", element->tstcorr_filt[i]);
	g_fprintf(fp, "\nA_pum Correction TF\nFrequency (Hz)\t\tReal\t\t\tImag\n");
	for(i = 0; i < element->pumcorr_tf_length; i++)
		g_fprintf(fp, "%f\t%1.15e\t%1.15e\n", (double) i * element->pum_sr / 2 / (element->pumcorr_tf_length - 1), creal(element->pumcorr_tf[i]), cimag(element->pumcorr_tf[i]));
	g_fprintf(fp, "\nA_pum Correction FIR Filter\n");
	for(i = 0; i < element->pum_fir_length; i++)
		g_fprintf(fp, "%1.15e\n", element->pumcorr_filt[i]);
	g_fprintf(fp, "\nA_uim Correction TF\nFrequency (Hz)\t\tReal\t\t\tImag\n");
	for(i = 0; i < element->uimcorr_tf_length; i++)
		g_fprintf(fp, "%f\t%1.15e\t%1.15e\n", (double) i * element->uim_sr / 2 / (element->uimcorr_tf_length - 1), creal(element->uimcorr_tf[i]), cimag(element->uimcorr_tf[i]));
	g_fprintf(fp, "\nA_uim Correction FIR Filter\n");
	for(i = 0; i < element->uim_fir_length; i++)
		g_fprintf(fp, "%1.15e\n", element->uimcorr_filt[i]);

	g_fprintf(fp, "\n\n");

	fclose(fp);
	g_free(element_name);
}


/*
 * Find the coefficients of R(f) = af^3 + bf^2 + cf + d, given 4 f values and 4 R values.
 */


static void find_cubic_coefficients(complex double *a, complex double *b, complex double *c, complex double *d, double f1, double f2, double f3, double f4, complex double R1, complex double R2, complex double R3, complex double R4) {

	double p, q, r, s;
	p = (f1 - f2) / (f2 - f3);
	q = (f2 - f3) / (f3 - f4);
	r = p * (f2 * f2 - f3 * f3) - (f1 * f1 - f2 * f2);
	s = q * (f3 * f3 - f4 * f4) - (f2 * f2 - f3 * f3);

	*a = (q * r * (R3 - R4) - (r + p * s) * (R2 - R3) + s * (R1 - R2)) / (q * r * (f3 * f3 * f3 - f4 * f4 * f4) - (r + p * s) * (f2 * f2 * f2 - f3 * f3 * f3) + s * (f1 * f1 * f1 - f2 * f2 * f2));

	*b = (p * (R2 - R3 - *a * (f2 * f2 * f2 - f3 * f3 * f3)) - (R1 - R2 - *a * (f1 * f1 * f1 - f2 * f2 * f2))) / (p * (f2 * f2 - f3 * f3) - (f1 * f1 - f2 * f2));

	*c = (R1 - R2 - *a * (f1 * f1 * f1 - f2 * f2 * f2) - *b * (f1 * f1 - f2 * f2)) / (f1 - f2);

	*d = R1 - *a * f1 * f1 * f1 - *b * f1 * f1 - *c * f1;

	return;
}


/*
 * Each time a measurement (measured_response, tst_tf, pum_tf, or uim_tf) is updated
 * and passed to this element, we update the correction TFs and filters.
 */


static GstFlowReturn update_calib_corr(GSTLALCalibCorr *element) {

	gint i, j, i1, i2, i3, i4;
	double p, q, f, df;
	complex double a, b, c, d;

	/* Start by creating current TDCF-corrected models for R, R*TST, R*PUM, and R*UIM. */
	for(i = 0; i < element->num_freqs; i++) {
		element->R_tdcf[i] = (1 + I * element->freqs[i] / element->fcc) / element->kc / element->cres_model[i] / element->freqs[i] / element->freqs[i] * (element->freqs[i] * element->freqs[i] + element->fs_squared - I * element->freqs[i] * element->fs_over_Q) + element->D[i] * (element->Ftst_model[i] * element->tst_model[i] * element->ktst * cexp(2 * M_PI * I * element->freqs[i] * element->tau_tst) + element->Ftst_model[i] * element->Fpum_model[i] * element->pum_model[i] * element->kpum * cexp(2 * M_PI * I * element->freqs[i] * element->tau_pum) + element->Ftst_model[i] * element->Fpum_model[i] * element->Fuim_model[i] * element->uim_model[i] * element->kuim * cexp(2 * M_PI * I * element->freqs[i] * element->tau_uim));
		element->tst_over_R_tdcf[i] = element->tst_model[i] * element->ktst * cexp(2 * M_PI * I * element->freqs[i] * element->tau_tst) / element->R_tdcf[i];
		element->pum_over_R_tdcf[i] = element->pum_model[i] * element->kpum * cexp(2 * M_PI * I * element->freqs[i] * element->tau_pum) / element->R_tdcf[i];
		element->uim_over_R_tdcf[i] = element->uim_model[i] * element->kuim * cexp(2 * M_PI * I * element->freqs[i] * element->tau_uim) / element->R_tdcf[i];
	}

	/*
	 * The measured values of R, R*TST, R*PUM, and R*UIM need to be interpolated
	 * to include every measurement frequency before further processing.  Use
	 * the above models to interpolate accurately: divide by the model to get a
	 * "boring" function, interpolate with a cubic spline, and multiply the
	 * result by the model.
	 */
	for(i = 0; i < element->num_cinv_freqs; i++) {
		element->cinv[element->cinv_indices[i]] = element->measured_response[i] / element->R_tdcf[element->cinv_indices[i]];
		g_print("cinv[%f] = %e + %ei\n", element->freqs[element->cinv_indices[i]], creal(element->cinv[element->cinv_indices[i]]), cimag(element->cinv[element->cinv_indices[i]]));
		element->cinv[element->cinv_indices[i]] = 1.0;
	}
	for(i = 0; i < element->num_tst_freqs; i++) {
		element->tst[element->tst_indices[i]] = element->tst_tf[i] / element->tst_over_R_tdcf[element->tst_indices[i]];
		g_print("tst[%f] = %e + %ei\n", element->freqs[element->tst_indices[i]], creal(element->tst[element->tst_indices[i]]), cimag(element->tst[element->tst_indices[i]]));
		element->tst[element->tst_indices[i]] = 1.0;
	}
	for(i = 0; i < element->num_pum_freqs; i++) {
		element->pum[element->pum_indices[i]] = element->pum_tf[i] / element->pum_over_R_tdcf[element->pum_indices[i]];
		g_print("pum[%f] = %e + %ei\n", element->freqs[element->pum_indices[i]], creal(element->pum[element->pum_indices[i]]), cimag(element->pum[element->pum_indices[i]]));
		element->pum[element->pum_indices[i]] = 1.0;
	}
	for(i = 0; i < element->num_uim_freqs; i++) {
		element->uim[element->uim_indices[i]] = element->uim_tf[i] / element->uim_over_R_tdcf[element->uim_indices[i]];
		g_print("uim[%f] = %e + %ei\n", element->freqs[element->uim_indices[i]], creal(element->uim[element->uim_indices[i]]), cimag(element->uim[element->uim_indices[i]]));
		element->uim[element->uim_indices[i]] = 1.0;
	}

	/*
	 * Interpolate each measurement to include all measurement frequencies.
	 * Extrapolate to the DC and Nyquist components by setting the DC and Nyquist
	 * components to reasonable values and interpolating.
	 */

	/* DC */
	element->cinv[0] = cabs(element->cinv[element->cinv_indices[0]]);
	element->tst[0] = cabs(element->tst[element->tst_indices[0]]);
	element->pum[0] = cabs(element->pum[element->pum_indices[0]]);
	element->uim[0] = cabs(element->uim[element->uim_indices[0]]);

	/* Nyquist */
	element->cinv[element->num_freqs - 1] = element->cinv[element->cinv_indices[element->num_cinv_freqs - 1]];
	element->tst[element->num_freqs - 1] = element->tst[element->tst_indices[element->num_tst_freqs - 1]];
	element->pum[element->num_freqs - 1] = element->pum[element->pum_indices[element->num_pum_freqs - 1]];
	element->uim[element->num_freqs - 1] = element->uim[element->uim_indices[element->num_uim_freqs - 1]];

	/* Measured response interpolation */
	find_cubic_coefficients(&a, &b, &c, &d, 0.0, element->cinv_freqs[0], element->cinv_freqs[1], element->cinv_freqs[2], element->cinv[0], element->cinv[element->cinv_indices[0]], element->cinv[element->cinv_indices[1]], element->cinv[element->cinv_indices[1]]);
	for(j = 1; j < element->cinv_indices[1]; j++) {
		f = element->freqs[j];
		element->cinv[j] = a * f * f * f + b * f * f + c * f + d;
	}
	for(i = 1; i < element->num_cinv_freqs - 2; i++) {
		find_cubic_coefficients(&a, &b, &c, &d, element->cinv_freqs[i - 1], element->cinv_freqs[i], element->cinv_freqs[i + 1], element->cinv_freqs[i + 2], element->cinv[element->cinv_indices[i - 1]], element->cinv[element->cinv_indices[i]], element->cinv[element->cinv_indices[i + 1]], element->cinv[element->cinv_indices[i + 2]]);
		for(j = element->cinv_indices[i] + 1; j < element->cinv_indices[i + 1]; j++) {
			f = element->freqs[j];
			element->cinv[j] = a * f * f * f + b * f * f + c * f + d;
		}
	}
	find_cubic_coefficients(&a, &b, &c, &d, element->cinv_freqs[element->num_cinv_freqs - 3], element->cinv_freqs[element->num_cinv_freqs - 2], element->cinv_freqs[element->num_cinv_freqs - 1], element->freqs[element->num_freqs - 1], element->cinv[element->cinv_indices[element->num_cinv_freqs - 3]], element->cinv[element->cinv_indices[element->num_cinv_freqs - 2]], element->cinv[element->cinv_indices[element->num_cinv_freqs - 1]], element->cinv[element->num_freqs - 1]);
	for(j = element->cinv_indices[element->num_cinv_freqs - 2] + 1; j < element->num_freqs - 1; j++) {
		f = element->freqs[j];
		element->cinv[j] = a * f * f * f + b * f * f + c * f + d;
	}

	/* TST interpolation */
	find_cubic_coefficients(&a, &b, &c, &d, 0.0, element->tst_freqs[0], element->tst_freqs[1], element->tst_freqs[2], element->tst[0], element->tst[element->tst_indices[0]], element->tst[element->tst_indices[1]], element->tst[element->tst_indices[1]]);
	for(j = 1; j < element->tst_indices[1]; j++) {
		f = element->freqs[j];
		element->tst[j] = a * f * f * f + b * f * f + c * f + d;
	}
	for(i = 1; i < element->num_tst_freqs - 2; i++) {
		find_cubic_coefficients(&a, &b, &c, &d, element->tst_freqs[i - 1], element->tst_freqs[i], element->tst_freqs[i + 1], element->tst_freqs[i + 2], element->tst[element->tst_indices[i - 1]], element->tst[element->tst_indices[i]], element->tst[element->tst_indices[i + 1]], element->tst[element->tst_indices[i + 2]]);
		for(j = element->tst_indices[i] + 1; j < element->tst_indices[i + 1]; j++) {
			f = element->freqs[j];
			element->tst[j] = a * f * f * f + b * f * f + c * f + d;
		}
	}
	find_cubic_coefficients(&a, &b, &c, &d, element->tst_freqs[element->num_tst_freqs - 3], element->tst_freqs[element->num_tst_freqs - 2], element->tst_freqs[element->num_tst_freqs - 1], element->freqs[element->num_freqs - 1], element->tst[element->tst_indices[element->num_tst_freqs - 3]], element->tst[element->tst_indices[element->num_tst_freqs - 2]], element->tst[element->tst_indices[element->num_tst_freqs - 1]], element->tst[element->num_freqs - 1]);
	for(j = element->tst_indices[element->num_tst_freqs - 2] + 1; j < element->num_freqs - 1; j++) {
		f = element->freqs[j];
		element->tst[j] = a * f * f * f + b * f * f + c * f + d;
	}

	/* PUM interpolation */
	find_cubic_coefficients(&a, &b, &c, &d, 0.0, element->pum_freqs[0], element->pum_freqs[1], element->pum_freqs[2], element->pum[0], element->pum[element->pum_indices[0]], element->pum[element->pum_indices[1]], element->pum[element->pum_indices[1]]);
        for(j = 1; j < element->pum_indices[1]; j++) {
                f = element->freqs[j];
                element->pum[j] = a * f * f * f + b * f * f + c * f + d;
        }
        for(i = 1; i < element->num_pum_freqs - 2; i++) {
                find_cubic_coefficients(&a, &b, &c, &d, element->pum_freqs[i - 1], element->pum_freqs[i], element->pum_freqs[i + 1], element->pum_freqs[i + 2], element->pum[element->pum_indices[i - 1]], element->pum[element->pum_indices[i]], element->pum[element->pum_indices[i + 1]], element->pum[element->pum_indices[i + 2]]);
                for(j = element->pum_indices[i] + 1; j < element->pum_indices[i + 1]; j++) { 
                        f = element->freqs[j];
                        element->pum[j] = a * f * f * f + b * f * f + c * f + d;
                }
        }
        find_cubic_coefficients(&a, &b, &c, &d, element->pum_freqs[element->num_pum_freqs - 3], element->pum_freqs[element->num_pum_freqs - 2], element->pum_freqs[element->num_pum_freqs - 1], element->freqs[element->num_freqs - 1], element->pum[element->pum_indices[element->num_pum_freqs - 3]], element->pum[element->pum_indices[element->num_pum_freqs - 2]], element->pum[element->pum_indices[element->num_pum_freqs - 1]], element->pum[element->num_freqs - 1]);
        for(j = element->pum_indices[element->num_pum_freqs - 2] + 1; j < element->num_freqs - 1; j++) { 
                f = element->freqs[j];
                element->pum[j] = a * f * f * f + b * f * f + c * f + d;
        }

	/* UIM interpolation */
	find_cubic_coefficients(&a, &b, &c, &d, 0.0, element->uim_freqs[0], element->uim_freqs[1], element->uim_freqs[2], element->uim[0], element->uim[element->uim_indices[0]], element->uim[element->uim_indices[1]], element->uim[element->uim_indices[1]]);
        for(j = 1; j < element->uim_indices[1]; j++) {
                f = element->freqs[j];
                element->uim[j] = a * f * f * f + b * f * f + c * f + d;
        }
        for(i = 1; i < element->num_uim_freqs - 2; i++) {
                find_cubic_coefficients(&a, &b, &c, &d, element->uim_freqs[i - 1], element->uim_freqs[i], element->uim_freqs[i + 1], element->uim_freqs[i + 2], element->uim[element->uim_indices[i - 1]], element->uim[element->uim_indices[i]], element->uim[element->uim_indices[i + 1]], element->uim[element->uim_indices[i + 2]]);
                for(j = element->uim_indices[i] + 1; j < element->uim_indices[i + 1]; j++) { 
                        f = element->freqs[j];
                        element->uim[j] = a * f * f * f + b * f * f + c * f + d;
                }
        }
        find_cubic_coefficients(&a, &b, &c, &d, element->uim_freqs[element->num_uim_freqs - 3], element->uim_freqs[element->num_uim_freqs - 2], element->uim_freqs[element->num_uim_freqs - 1], element->freqs[element->num_freqs - 1], element->uim[element->uim_indices[element->num_uim_freqs - 3]], element->uim[element->uim_indices[element->num_uim_freqs - 2]], element->uim[element->uim_indices[element->num_uim_freqs - 1]], element->uim[element->num_freqs - 1]);
        for(j = element->uim_indices[element->num_uim_freqs - 2] + 1; j < element->num_freqs - 1; j++) { 
                f = element->freqs[j];
                element->uim[j] = a * f * f * f + b * f * f + c * f + d;
        }

	for(i = 0; i < element->num_freqs; i++) {
		g_print("cinv[%f] = %e + %ei\n", element->freqs[i], creal(element->cinv[i]), cimag(element->cinv[i]));
		g_print("tst[%f] = %e + %ei\n", element->freqs[i], creal(element->tst[i]), cimag(element->tst[i]));
		g_print("pum[%f] = %e + %ei\n", element->freqs[i], creal(element->pum[i]), cimag(element->pum[i]));
		g_print("uim[%f] = %e + %ei\n", element->freqs[i], creal(element->uim[i]), cimag(element->uim[i]));
		/* Convert back to measured R(f) and A_i(f) */
		element->cinv[i] *= element->R_tdcf[i];
		element->tst[i] *= element->tst_over_R_tdcf[i] * element->cinv[i];
		element->pum[i] *= element->pum_over_R_tdcf[i] * element->cinv[i];
		element->uim[i] *= element->uim_over_R_tdcf[i] * element->cinv[i];

		/* Find measured inverse sensing function */
		element->cinv[i] -= element->D[i] * (element->Ftst_model[i] * element->tst[i] + element->Ftst_model[i] * element->Fpum_model[i] * element->pum[i] + element->Ftst_model[i] * element->Fpum_model[i] * element->Fuim_model[i] * element->uim[i]);

		/* Compute the corrections */
		element->cinv[i] /= (1 + I * element->freqs[i] / element->fcc) / element->kc / element->cres_model[i] / element->freqs[i] / element->freqs[i] * (element->freqs[i] * element->freqs[i] + element->fs_squared - I * element->freqs[i] * element->fs_over_Q);
		element->tst[i] /= element->tst_model[i] * element->ktst * cexp(2 * M_PI * I * element->freqs[i] * element->tau_tst);
		element->pum[i] /= element->pum_model[i] * element->kpum * cexp(2 * M_PI * I * element->freqs[i] * element->tau_pum);
		element->uim[i] /= element->uim_model[i] * element->kuim * cexp(2 * M_PI * I * element->freqs[i] * element->tau_uim);
	}

	/* Resample inverse sensing correction to make an evenly-spaced TF from DC to Nyquist. */
	element->cinvcorr_tf[0] = cabs(element->cinv[0]);
	i1 = 0;
	i2 = i1 + 1;
	df = (double) element->cinv_sr / 2 / (element->cinvcorr_tf_length - 1);
	/* For interpolation, require that the input spacing is greater than the output spacing. */
	while(i2 < element->num_freqs - 1 && element->freqs[i2] - element->freqs[i1] < df)
		i2++;
	i3 = i2 + 1;
	while(i3 < element->num_freqs - 1 && element->freqs[i3] - element->freqs[i2] < df)
		i3++;
	i4 = i3 + 1;
	while(i4 < element->num_freqs - 1 && element->freqs[i4] - element->freqs[i3] < df)
		i4++;
	find_cubic_coefficients(&a, &b, &c, &d, element->freqs[i1], element->freqs[i2], element->freqs[i3], element->freqs[i4], element->cinv[i1], element->cinv[i2], element->cinv[i3], element->cinv[i4]);
	j = 1;
	f = df;
	while(f <= element->freqs[i3] && j < element->cinvcorr_tf_length) {
		element->cinvcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
		j++;
		f = (double) j * element->cinv_sr / 2 / (element->cinvcorr_tf_length - 1);
	}
	while(j < element->cinvcorr_tf_length) {
		if(i4 < element->num_freqs - 1) {
			i1 = i2;
			i2 = i3;
			i3 = i4;
			i4++;
			while(i4 < element->num_freqs - 1 && element->freqs[i4] - element->freqs[i3] < df)
				i4++;
			find_cubic_coefficients(&a, &b, &c, &d, element->freqs[i1], element->freqs[i2], element->freqs[i3], element->freqs[i4], element->cinv[i1], element->cinv[i2], element->cinv[i3], element->cinv[i4]);
			while(f <= element->freqs[i3] && j < element->cinvcorr_tf_length) {
				element->cinvcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
				j++;
				f = (double) j * element->cinv_sr / 2 / (element->cinvcorr_tf_length - 1);
			}
		} else {
			while(j < element->cinvcorr_tf_length) {
				element->cinvcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
				j++;
				f = (double) j * element->cinv_sr / 2 / (element->cinvcorr_tf_length - 1);
			}
		}
	}
	if(!element->cinv_fir_length % 2)
		element->cinvcorr_tf[element->cinvcorr_tf_length - 1] = cabs(element->cinvcorr_tf[element->cinvcorr_tf_length - 1]);

	/* Resample tst correction to make an evenly-spaced TF from DC to Nyquist. */
	element->tstcorr_tf[0] = cabs(element->tst[0]);
	i1 = 0;
	i2 = i1 + 1;
	df = (double) element->tst_sr / 2 / (element->tstcorr_tf_length - 1);
	/* For interpolation, require that the input spacing is greater than the output spacing. */
	while(i2 < element->num_freqs - 1 && element->freqs[i2] - element->freqs[i1] < df)
		i2++;
	i3 = i2 + 1;
	while(i3 < element->num_freqs - 1 && element->freqs[i3] - element->freqs[i2] < df)
		i3++;
	i4 = i3 + 1;
	while(i4 < element->num_freqs - 1 && element->freqs[i4] - element->freqs[i3] < df)
		i4++;
	find_cubic_coefficients(&a, &b, &c, &d, element->freqs[i1], element->freqs[i2], element->freqs[i3], element->freqs[i4], element->tst[i1], element->tst[i2], element->tst[i3], element->tst[i4]);
	j = 1;
	f = df;
	while(f <= element->freqs[i3] && j < element->tstcorr_tf_length) {
		element->tstcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
		j++;
		f = (double) j * element->tst_sr / 2 / (element->tstcorr_tf_length - 1);
	}
	while(j < element->tstcorr_tf_length) {
		if(i4 < element->num_freqs - 1) {
			i1 = i2;
			i2 = i3;
			i3 = i4;
			i4++;
			while(i4 < element->num_freqs - 1 && element->freqs[i4] - element->freqs[i3] < df)
				i4++;
			find_cubic_coefficients(&a, &b, &c, &d, element->freqs[i1], element->freqs[i2], element->freqs[i3], element->freqs[i4], element->tst[i1], element->tst[i2], element->tst[i3], element->tst[i4]);
			while(f <= element->freqs[i3] && j < element->tstcorr_tf_length) {
				element->tstcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
				j++;
				f = (double) j * element->tst_sr / 2 / (element->tstcorr_tf_length - 1);
			}
		} else {
			while(j < element->tstcorr_tf_length) {
				element->tstcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
				j++;
				f = (double) j * element->tst_sr / 2 / (element->tstcorr_tf_length - 1);
			}
		}
	}
	if(!element->tst_fir_length % 2)
		element->tstcorr_tf[element->tstcorr_tf_length - 1] = cabs(element->tstcorr_tf[element->tstcorr_tf_length - 1]);

	/* Resample pum correction to make an evenly-spaced TF from DC to Nyquist. */
	element->pumcorr_tf[0] = cabs(element->pum[0]);
	i1 = 0;
	i2 = i1 + 1;
	df = (double) element->pum_sr / 2 / (element->pumcorr_tf_length - 1);
	/* For interpolation, require that the input spacing is greater than the output spacing. */
	while(i2 < element->num_freqs - 1 && element->freqs[i2] - element->freqs[i1] < df)
		i2++;
	i3 = i2 + 1;
	while(i3 < element->num_freqs - 1 && element->freqs[i3] - element->freqs[i2] < df)
		i3++;
	i4 = i3 + 1;
	while(i4 < element->num_freqs - 1 && element->freqs[i4] - element->freqs[i3] < df)
		i4++;
	find_cubic_coefficients(&a, &b, &c, &d, element->freqs[i1], element->freqs[i2], element->freqs[i3], element->freqs[i4], element->pum[i1], element->pum[i2], element->pum[i3], element->pum[i4]);
	j = 1;
	f = df;
	while(f <= element->freqs[i3] && j < element->pumcorr_tf_length) {
		element->pumcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
		j++;
		f = (double) j * element->pum_sr / 2 / (element->pumcorr_tf_length - 1);
	}
	while(j < element->pumcorr_tf_length) {
		if(i4 < element->num_freqs - 1) {
			i1 = i2;
			i2 = i3;
			i3 = i4;
			i4++;
			while(i4 < element->num_freqs - 1 && element->freqs[i4] - element->freqs[i3] < df)
				i4++;
			find_cubic_coefficients(&a, &b, &c, &d, element->freqs[i1], element->freqs[i2], element->freqs[i3], element->freqs[i4], element->pum[i1], element->pum[i2], element->pum[i3], element->pum[i4]);
			while(f <= element->freqs[i3] && j < element->pumcorr_tf_length) {
				element->pumcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
				j++;
				f = (double) j * element->pum_sr / 2 / (element->pumcorr_tf_length - 1);
			}
		} else {
			while(j < element->pumcorr_tf_length) {
				element->pumcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
				j++;
				f = (double) j * element->pum_sr / 2 / (element->pumcorr_tf_length - 1);
			}
		}
	}
	if(!element->pum_fir_length % 2)
		element->pumcorr_tf[element->pumcorr_tf_length - 1] = cabs(element->pumcorr_tf[element->pumcorr_tf_length - 1]);

	/* Resample uim correction to make an evenly-spaced TF from DC to Nyquist. */
	element->uimcorr_tf[0] = cabs(element->uim[0]);
	i1 = 0;
	i2 = i1 + 1;
	df = (double) element->uim_sr / 2 / (element->uimcorr_tf_length - 1);
	/* For interpolation, require that the input spacing is greater than the output spacing. */
	while(i2 < element->num_freqs - 1 && element->freqs[i2] - element->freqs[i1] < df)
		i2++;
	i3 = i2 + 1;
	while(i3 < element->num_freqs - 1 && element->freqs[i3] - element->freqs[i2] < df)
		i3++;
	i4 = i3 + 1;
	while(i4 < element->num_freqs - 1 && element->freqs[i4] - element->freqs[i3] < df)
		i4++;
	find_cubic_coefficients(&a, &b, &c, &d, element->freqs[i1], element->freqs[i2], element->freqs[i3], element->freqs[i4], element->uim[i1], element->uim[i2], element->uim[i3], element->uim[i4]);
	j = 1;
	f = df;
	while(f <= element->freqs[i3] && j < element->uimcorr_tf_length) {
		element->uimcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
		j++;
		f = (double) j * element->uim_sr / 2 / (element->uimcorr_tf_length - 1);
	}
	while(j < element->uimcorr_tf_length) {
		if(i4 < element->num_freqs - 1) {
			i1 = i2;
			i2 = i3;
			i3 = i4;
			i4++;
			while(i4 < element->num_freqs - 1 && element->freqs[i4] - element->freqs[i3] < df)
				i4++;
			find_cubic_coefficients(&a, &b, &c, &d, element->freqs[i1], element->freqs[i2], element->freqs[i3], element->freqs[i4], element->uim[i1], element->uim[i2], element->uim[i3], element->uim[i4]);
			while(f <= element->freqs[i3] && j < element->uimcorr_tf_length) {
				element->uimcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
				j++;
				f = (double) j * element->uim_sr / 2 / (element->uimcorr_tf_length - 1);
			}
		} else {
			while(j < element->uimcorr_tf_length) {
				element->uimcorr_tf[j] = a * f * f * f + b * f * f + c * f + d;
				j++;
				f = (double) j * element->uim_sr / 2 / (element->uimcorr_tf_length - 1);
			}
		}
	}
	if(!element->uim_fir_length % 2)
		element->uimcorr_tf[element->uimcorr_tf_length - 1] = cabs(element->uimcorr_tf[element->uimcorr_tf_length - 1]);

	/* Finally, make the FIR filters, starting with inverse sensing */
	if(element->cinvcorr_filt)
		g_free(element->cinvcorr_filt);
	element->cinvcorr_filt = gstlal_irfft_double(element->cinvcorr_tf, (guint) element->cinvcorr_tf_length, NULL, NULL, 0, NULL, TRUE, 0, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, FALSE);
	/* Center it in time by shifting samples N / 2 spaces in memory to the right */
	if(element->cinv_fir_length % 2) {
		/* Odd-length filter */
		p = element->cinvcorr_filt[0];
		for(i = 1; i <= element->cinv_fir_length; i++) {
			q = element->cinvcorr_filt[(i * (element->cinv_fir_length / 2)) % element->cinv_fir_length];
			element->cinvcorr_filt[(i * (element->cinv_fir_length / 2)) % element->cinv_fir_length] = p;
			p = q;
		}
	} else {
		/* Even-length filter */
		for(i = 0; i < element->cinv_fir_length / 2; i++) {
			p = element->cinvcorr_filt[i];
			element->cinvcorr_filt[i] = element->cinvcorr_filt[i + element->cinv_fir_length / 2];
			element->cinvcorr_filt[i + element->cinv_fir_length / 2] = p;
		}
	}

	/* Window */
	for(i = 0; i < element->cinv_fir_length; i++)
		element->cinvcorr_filt[i] *= element->cinv_window[i];

	/* tst FIR filter */
	if(element->tstcorr_filt)
		g_free(element->tstcorr_filt);
	element->tstcorr_filt = gstlal_irfft_double(element->tstcorr_tf, (guint) element->tstcorr_tf_length, NULL, NULL, 0, NULL, TRUE, 0, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, FALSE);
	/* Center it in time by shifting samples N / 2 spaces in memory to the right */
	if(element->tst_fir_length % 2) {
		/* Odd-length filter */
		p = element->tstcorr_filt[0];
		for(i = 1; i <= element->tst_fir_length; i++) {
			q = element->tstcorr_filt[(i * (element->tst_fir_length / 2)) % element->tst_fir_length];
			element->tstcorr_filt[(i * (element->tst_fir_length / 2)) % element->tst_fir_length] = p;
			p = q;
		}
	} else {
		/* Even-length filter */
		for(i = 0; i < element->tst_fir_length / 2; i++) {
			p = element->tstcorr_filt[i];
			element->tstcorr_filt[i] = element->tstcorr_filt[i + element->tst_fir_length / 2];
			element->tstcorr_filt[i + element->tst_fir_length / 2] = p;
		}
	}

	/* Window */
	for(i = 0; i < element->tst_fir_length; i++)
		element->tstcorr_filt[i] *= element->tst_window[i];

	/* pum FIR filter */
	if(element->pumcorr_filt)
		g_free(element->pumcorr_filt);
	element->pumcorr_filt = gstlal_irfft_double(element->pumcorr_tf, (guint) element->pumcorr_tf_length, NULL, NULL, 0, NULL, TRUE, 0, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, FALSE);
	/* Center it in time by shifting samples N / 2 spaces in memory to the right */
	if(element->pum_fir_length % 2) {
		/* Odd-length filter */
		p = element->pumcorr_filt[0];
		for(i = 1; i <= element->pum_fir_length; i++) {
			q = element->pumcorr_filt[(i * (element->pum_fir_length / 2)) % element->pum_fir_length];
			element->pumcorr_filt[(i * (element->pum_fir_length / 2)) % element->pum_fir_length] = p; 
			p = q;
		}
	} else {
		/* Even-length filter */
		for(i = 0; i < element->pum_fir_length / 2; i++) {
			p = element->pumcorr_filt[i];
			element->pumcorr_filt[i] = element->pumcorr_filt[i + element->pum_fir_length / 2];
			element->pumcorr_filt[i + element->pum_fir_length / 2] = p;
		}
	}

	/* Window */
	for(i = 0; i < element->pum_fir_length; i++)
		element->pumcorr_filt[i] *= element->pum_window[i];

	/* uim FIR filter */
	if(element->uimcorr_filt)
		g_free(element->uimcorr_filt);
	element->uimcorr_filt = gstlal_irfft_double(element->uimcorr_tf, (guint) element->uimcorr_tf_length, NULL, NULL, 0, NULL, TRUE, 0, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, FALSE);
	/* Center it in time by shifting samples N / 2 spaces in memory to the right */
	if(element->uim_fir_length % 2) {
		/* Odd-length filter */
		p = element->uimcorr_filt[0];
		for(i = 1; i <= element->uim_fir_length; i++) {
			q = element->uimcorr_filt[(i * (element->uim_fir_length / 2)) % element->uim_fir_length];
			element->uimcorr_filt[(i * (element->uim_fir_length / 2)) % element->uim_fir_length] = p; 
			p = q;
		}
	} else {
		/* Even-length filter */
		for(i = 0; i < element->uim_fir_length / 2; i++) {
			p = element->uimcorr_filt[i];
			element->uimcorr_filt[i] = element->uimcorr_filt[i + element->uim_fir_length / 2];
			element->uimcorr_filt[i + element->uim_fir_length / 2] = p;
		}
	}

	/* Window */
	for(i = 0; i < element->uim_fir_length; i++)
		element->uimcorr_filt[i] *= element->uim_window[i];

	GST_LOG_OBJECT(element, "Just computed new FIR filters");
	/* Let other elements know about the update */
	g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_CINVCORR_TF]);
	g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_CINVCORR_FILT]);
	g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_TSTCORR_TF]);
	g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_TSTCORR_FILT]);
	g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_PUMCORR_TF]);
	g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_PUMCORR_FILT]);
	g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_UIMCORR_TF]);
	g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_UIMCORR_FILT]);
	/* Write transfer functions and FIR filters to a file if we want */
	if(element->filename) {
		write_filters(element);
	}

	return GST_FLOW_OK;
}


/*
 * ============================================================================
 *
 *			    GstBaseSink Overrides
 *
 * ============================================================================
 */


/*
 * start()
 */


static gboolean start(GstBaseSink *sink) {

	GSTLALCalibCorr *element = GSTLAL_CALIBCORR(sink);

	/* Timestamp bookkeeping */
	element->pts = element->t0 = GST_CLOCK_TIME_NONE;
	element->offset0 = GST_BUFFER_OFFSET_NONE;
	element->next_in_offset = GST_BUFFER_OFFSET_NONE;

	/* If we are writing output to file, and a file already exists with the same name, remove it */
	if(element->filename)
		remove(element->filename);

	g_mutex_init(&element->cinv_lock);
	g_mutex_init(&element->tst_lock);
	g_mutex_init(&element->pum_lock);
	g_mutex_init(&element->uim_lock);

	element->ktst = 1.0;
	element->tau_tst = 0.0;
	element->kpum = 1.0;
	element->tau_pum = 0.0;
	element->kuim = 1.0;
	element->tau_uim = 0.0;
	element->kc = 1.0;
	element->fcc = 400;
	element->fs_squared = 0.0;
	element->fs_over_Q = 0.0;

	return TRUE;
}


/*
 * set_caps()
 */


static gboolean set_caps(GstBaseSink *sink, GstCaps *caps) {

	GSTLALCalibCorr *element = GSTLAL_CALIBCORR(sink);

	int i, j, k, l;
	double freq;

	gboolean success = TRUE;

	/* Parse the caps to find the format, sample rate, and number of channels */
	GstStructure *str = gst_caps_get_structure(caps, 0);
	success &= gst_structure_get_int(str, "rate", &element->rate);

	/*
	 * All the injection frequencies for Pcal, TST, PUM, and UIM
	 */

	if(element->freqs) {
		g_free(element->freqs);
		element->freqs = NULL;
	}

	/* Find the total number of measurement frequencies.  For simplicity, allocate maximum memory that could be needed. */
	element->freqs = g_malloc((2 + element->num_cinv_freqs + element->num_tst_freqs + element->num_pum_freqs + element->num_uim_freqs) * sizeof(double));
	i = 0;
	j = 0;
	k = 0;
	l = 0;
	freq = 0.01;
	element->freqs[0] = 0.01;
	element->num_freqs = 1;
	while(i < element->num_cinv_freqs || j < element->num_tst_freqs || k < element->num_pum_freqs || l < element->num_uim_freqs) {
		if(i < element->num_cinv_freqs) {
			if(element->cinv_freqs[i] > freq && (j >= element->num_tst_freqs || element->cinv_freqs[i] <= element->tst_freqs[j]) && (k >= element->num_pum_freqs || element->cinv_freqs[i] <= element->pum_freqs[k]) && (l >= element->num_uim_freqs || element->cinv_freqs[i] <= element->uim_freqs[l])) {
				element->freqs[element->num_freqs] = freq = element->cinv_freqs[i];
				element->cinv_indices[i] = element->num_freqs;
				element->num_freqs++;
				i++;
			} else if(element->cinv_freqs[i] == freq) {
				if(i > 0) {
					if(element->cinv_freqs[i] == element->cinv_freqs[i - 1]) {
						GST_ERROR_OBJECT(element, "cinv-freqs must not contain duplicates");
						g_assert_not_reached();
					} else {
						element->cinv_indices[i] = element->num_freqs;
						i++;
					}
				} else {
					element->cinv_indices[i] = element->num_freqs;
					i++;
				}
			} else if(element->cinv_freqs[i] < freq) {
				GST_ERROR_OBJECT(element, "cinv-freqs must be given in increasing order");
				g_assert_not_reached();
			}
		}
		if(j < element->num_tst_freqs) {
			if(element->tst_freqs[j] > freq && (i >= element->num_cinv_freqs || element->tst_freqs[j] <= element->cinv_freqs[i]) && (k >= element->num_pum_freqs || element->tst_freqs[j] <= element->pum_freqs[k]) && (l >= element->num_uim_freqs || element->tst_freqs[j] <= element->uim_freqs[l])) {
				element->freqs[element->num_freqs] = freq = element->tst_freqs[j];
				element->tst_indices[j] = element->num_freqs;
				element->num_freqs++;
				j++;
			} else if(element->tst_freqs[j] == freq) {
				if(j > 0) {
					if(element->tst_freqs[j] == element->tst_freqs[j - 1]) {
						GST_ERROR_OBJECT(element, "tst-freqs must not contain duplicates");
						g_assert_not_reached();
					} else {
						element->tst_indices[j] = element->num_freqs;
						j++;
					}
				} else {
					element->tst_indices[j] = element->num_freqs;
					j++;
				}
			} else if(element->tst_freqs[j] < freq) {
				GST_ERROR_OBJECT(element, "tst-freqs must be non-negative and given in increasing order");
				g_assert_not_reached();
			}
		}
		if(k < element->num_pum_freqs) {
			if(element->pum_freqs[k] > freq && (i >= element->num_cinv_freqs || element->pum_freqs[k] <= element->cinv_freqs[i]) && (j >= element->num_tst_freqs || element->pum_freqs[k] <= element->tst_freqs[j]) && (l >= element->num_uim_freqs || element->pum_freqs[k] <= element->uim_freqs[l])) {
				element->freqs[element->num_freqs] = freq = element->pum_freqs[k];
				element->pum_indices[k] = element->num_freqs;
				element->num_freqs++;
				k++;
			} else if(element->pum_freqs[k] == freq) {
				if(k > 0) {
					if(element->pum_freqs[k] == element->pum_freqs[k - 1]) {
						GST_ERROR_OBJECT(element, "pum-freqs must not contain duplicates");
						g_assert_not_reached();
					} else {
						element->pum_indices[k] = element->num_freqs;
						k++;
					}
				} else {
					element->pum_indices[k] = element->num_freqs;
					k++;
				}
			} else if(element->pum_freqs[k] < freq) {
				GST_ERROR_OBJECT(element, "pum-freqs must be non-negative and given in increasing order");
				g_assert_not_reached();
			}
		}
		if(l < element->num_uim_freqs) {
			if(element->uim_freqs[l] > freq && (i >= element->num_cinv_freqs || element->uim_freqs[l] <= element->cinv_freqs[i]) && (k >= element->num_pum_freqs || element->uim_freqs[l] <= element->pum_freqs[k]) && (j >= element->num_tst_freqs || element->uim_freqs[l] <= element->tst_freqs[j])) {
				element->freqs[element->num_freqs] = freq = element->uim_freqs[l];
				element->uim_indices[l] = element->num_freqs;
				element->num_freqs++;
				l++;
			} else if(element->uim_freqs[l] == freq) {
				if(l > 0) {
					if(element->uim_freqs[l] == element->uim_freqs[l - 1]) {
						GST_ERROR_OBJECT(element, "uim-freqs must not contain duplicates");
						g_assert_not_reached();
					} else {
						element->uim_indices[l] = element->num_freqs;
						l++;
					}
				} else {
					element->uim_indices[l] = element->num_freqs;
					l++;
				}
			} else if(element->uim_freqs[l] < freq) {
				GST_ERROR_OBJECT(element, "uim-freqs must be non-negative and given in increasing order");
				g_assert_not_reached();
			}
		}
	}

	element->freqs[element->num_freqs] = (double) ((element->cinv_sr > element->tst_sr ? element->cinv_sr : element->tst_sr) > (element->pum_sr > element->uim_sr ? element->pum_sr : element->uim_sr) ? (element->cinv_sr > element->tst_sr ? element->cinv_sr : element->tst_sr) : (element->pum_sr > element->uim_sr ? element->pum_sr : element->uim_sr)) / 2;
	element->num_freqs++;

	element->cinv = g_malloc(element->num_freqs * sizeof(complex double));
	element->tst = g_malloc(element->num_freqs * sizeof(complex double));
	element->pum = g_malloc(element->num_freqs * sizeof(complex double));
	element->uim = g_malloc(element->num_freqs * sizeof(complex double));

	/* Requirements for resampling models, etc. */
	if(element->cres_model_length != element->num_freqs) {
		GST_ERROR_OBJECT(element, "Length of cres-model must equal total number of measurement frequencies (%d != %d).", element->cres_model_length, element->num_freqs);
		g_assert_not_reached();
	}
	if(element->measured_response_length != element->num_cinv_freqs) {
		GST_ERROR_OBJECT(element, "Length of measured-response must equal length of cinv-freqs (%d != %d).", element->measured_response_length, element->num_cinv_freqs);
		g_assert_not_reached();
	}
	if(element->D_length != element->num_freqs) {
		GST_ERROR_OBJECT(element, "Length of D must equal total number of measurement frequencies (%d != %d).", element->D_length, element->num_freqs);
		g_assert_not_reached();
	}
	if(element->Ftst_model_length != element->num_freqs) {
		GST_ERROR_OBJECT(element, "Length of Ftst-model must equal total number of measurement frequencies (%d != %d).", element->Ftst_model_length, element->num_freqs);
		g_assert_not_reached();
	}
	if(element->tst_model_length != element->num_freqs) {
		GST_ERROR_OBJECT(element, "Length of tst-model must equal total number of measurement frequencies (%d != %d).", element->tst_model_length, element->num_freqs);
		g_assert_not_reached();
	}
	if(element->tst_tf_length != element->num_tst_freqs) {
		GST_ERROR_OBJECT(element, "Length of tst-tf must equal length of tst-freqs (%d != %d).", element->tst_tf_length, element->num_tst_freqs);
		g_assert_not_reached();
	}
	if(element->Fpum_model_length != element->num_freqs) {
		GST_ERROR_OBJECT(element, "Length of Fpum-model must equal total number of measurement frequencies (%d != %d).", element->Fpum_model_length, element->num_freqs);
		g_assert_not_reached();
	}
	if(element->pum_model_length != element->num_freqs) {
		GST_ERROR_OBJECT(element, "Length of pum-model must equal total number of measurement frequencies (%d != %d).", element->pum_model_length, element->num_freqs);
		g_assert_not_reached();
	}
	if(element->pum_tf_length != element->num_pum_freqs) {
		GST_ERROR_OBJECT(element, "Length of pum-tf must equal length of pum-freqs (%d != %d).", element->pum_tf_length, element->num_pum_freqs);
		g_assert_not_reached();
	}
	if(element->Fuim_model_length != element->num_freqs) {
		GST_ERROR_OBJECT(element, "Length of Fuim-model must equal total number of measurement frequencies (%d != %d).", element->Fuim_model_length, element->num_freqs);
		g_assert_not_reached();
	}
	if(element->uim_model_length != element->num_freqs) {
		GST_ERROR_OBJECT(element, "Length of uim-model must equal total number of measurement frequencies (%d != %d).", element->uim_model_length, element->num_freqs);
		g_assert_not_reached();
	}
	if(element->uim_tf_length != element->num_uim_freqs) {
		GST_ERROR_OBJECT(element, "Length of uim-tf must equal length of uim-freqs (%d != %d).", element->uim_tf_length, element->num_uim_freqs);
		g_assert_not_reached();
	}

	return success;
}


/*
 * render()
 */


static GstFlowReturn render(GstBaseSink *sink, GstBuffer *buffer) {

	GSTLALCalibCorr *element = GSTLAL_CALIBCORR(sink);
	GstMapInfo mapinfo;
	GstFlowReturn result = GST_FLOW_OK;
	double *tdcfs;

	/*
	 * check for discontinuity
	 */

	if(G_UNLIKELY(GST_BUFFER_IS_DISCONT(buffer) || GST_BUFFER_OFFSET(buffer) != element->next_in_offset || !GST_CLOCK_TIME_IS_VALID(element->t0))) {
		element->t0 = GST_BUFFER_PTS(buffer);
		element->offset0 = GST_BUFFER_OFFSET(buffer);
	}
	element->pts = GST_BUFFER_PTS(buffer);
	element->next_in_offset = GST_BUFFER_OFFSET_END(buffer);
	GST_DEBUG_OBJECT(element, "have buffer spanning %" GST_BUFFER_BOUNDARIES_FORMAT, GST_BUFFER_BOUNDARIES_ARGS(buffer));

	/* Update the element with the latest input if it is valid */
	if(!GST_BUFFER_FLAG_IS_SET(buffer, GST_BUFFER_FLAG_GAP) && mapinfo.size != 0) {
		gst_buffer_map(buffer, &mapinfo, GST_MAP_READ);

		tdcfs = (double *) mapinfo.data;
		element->ktst = tdcfs[0];
		element->tau_tst = tdcfs[1];
		element->kpum = tdcfs[2];
		element->tau_pum = tdcfs[3];
		element->kuim = tdcfs[4];
		element->tau_uim = tdcfs[5];
		element->kc = tdcfs[6];
		element->fcc = tdcfs[7];
		element->fs_squared = tdcfs[8];
		element->fs_over_Q = tdcfs[9];

		gst_buffer_unmap(buffer, &mapinfo);
	}

	/* Update the correction filters only if we need to */
	if(element->need_update) {
		/* Wait a few seconds in case other updates are coming at the same time */
		sleep(5);
		g_mutex_lock(&element->cinv_lock);
		g_mutex_lock(&element->tst_lock);
		g_mutex_lock(&element->pum_lock);
		g_mutex_lock(&element->uim_lock);

		result = update_calib_corr(element);
		element->need_update = FALSE;

		g_mutex_unlock(&element->cinv_lock);
		g_mutex_unlock(&element->tst_lock);
		g_mutex_unlock(&element->pum_lock);
		g_mutex_unlock(&element->uim_lock);
	}

	return result;
}


/*
 * ============================================================================
 *
 *			      GObject Methods
 *
 * ============================================================================
 */


/*
 * properties
 */


static void set_property(GObject *object, enum property id, const GValue *value, GParamSpec *pspec) {

	GSTLALCalibCorr *element = GSTLAL_CALIBCORR(object);

	int k, length;
	double *double_prop;

	GST_OBJECT_LOCK(element);

	switch(id) {

	case ARG_CINV_FREQS:
		if(element->cinv_freqs) {
			g_free(element->cinv_freqs);
			element->cinv_freqs = NULL;
		}
		if(element->cinv_indices) {
			g_free(element->cinv_indices);
			element->cinv_indices = NULL;
		}
		element->measured_response_length = element->num_cinv_freqs = gst_value_array_get_size(value);
		element->cinv_freqs = g_malloc(element->num_cinv_freqs * sizeof(double));
		element->cinv_indices = g_malloc(element->num_cinv_freqs * sizeof(int));
		for(k = 0; k < element->num_cinv_freqs; k++)
			element->cinv_freqs[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_CINV_SR:
		element->cinv_sr = g_value_get_int(value);
		break;

	case ARG_CINV_FIR_LENGTH:
		element->cinv_fir_length = g_value_get_int(value);
		element->cinvcorr_tf_length = element->cinv_fir_length / 2 + 1;
		if(element->cinv_window)
			g_free(element->cinv_window);
		element->cinv_window = blackman_double(element->cinv_fir_length, NULL, FALSE);
		if(element->cinvcorr_tf)
			g_free(element->cinvcorr_tf);
		element->cinvcorr_tf = g_malloc(element->cinvcorr_tf_length * sizeof(*element->cinvcorr_tf));
		break;

	case ARG_CRES_MODEL:
		if(element->cres_model)
			g_free(element->cres_model);
		if(element->R_tdcf)
			g_free(element->R_tdcf);
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of cres-model (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		element->cres_model_length = length / 2;
		element->cres_model = g_malloc(length * sizeof(double));
		element->R_tdcf = g_malloc(length * sizeof(double));
		double_prop = (double *) element->cres_model;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_MEASURED_RESPONSE:
		g_mutex_lock(&element->cinv_lock);
		if(element->measured_response) {
			g_free(element->measured_response);
			element->measured_response = NULL;
		}
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of measured-response (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		if(element->measured_response_length == 0)
			element->measured_response_length = length / 2;
		else
			g_assert_cmpint(element->measured_response_length, ==, length / 2);
		element->measured_response = g_malloc(length * sizeof(double));
		double_prop = (double *) element->measured_response;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		/* Only apply an update if we have all we need */
		if(element->tst_tf && element->pum_tf && element->uim_tf)
			element->need_update = TRUE;
		g_mutex_unlock(&element->cinv_lock);
		break;

	case ARG_D:
		if(element->D) {
			g_free(element->D);
			element->D = NULL;
		}
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of D (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		element->D_length = length / 2;
		element->D = g_malloc(length * sizeof(double));
		double_prop = (double *) element->D;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_TST_FREQS:
		if(element->tst_freqs) {
			g_free(element->tst_freqs);
			element->tst_freqs = NULL;
		}
		if(element->tst_indices) {
			g_free(element->tst_indices);
			element->tst_indices = NULL;
		}
		element->tst_tf_length = element->num_tst_freqs = gst_value_array_get_size(value);
		element->tst_freqs = g_malloc(element->num_tst_freqs * sizeof(double));
		element->tst_indices = g_malloc(element->num_tst_freqs * sizeof(int));
		for(k = 0; k < element->num_tst_freqs; k++)
			element->tst_freqs[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_TST_SR:
		element->tst_sr = g_value_get_int(value);
		break;

	case ARG_TST_FIR_LENGTH:
		element->tst_fir_length = g_value_get_int(value);
		element->tstcorr_tf_length = element->tst_fir_length / 2 + 1;
		if(element->tst_window)
			g_free(element->tst_window);
		element->tst_window = blackman_double(element->tst_fir_length, NULL, FALSE);
		if(element->tstcorr_tf)
			g_free(element->tstcorr_tf);
		element->tstcorr_tf = g_malloc(element->tstcorr_tf_length * sizeof(*element->tstcorr_tf));
		break;

	case ARG_FTST_MODEL:
		if(element->Ftst_model)
			g_free(element->Ftst_model);
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of Ftst-model (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		element->Ftst_model_length = length / 2;
		element->Ftst_model = g_malloc(length * sizeof(double));
		double_prop = (double *) element->Ftst_model;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_TST_MODEL:
		if(element->tst_model)
			g_free(element->tst_model);
		if(element->tst_over_R_tdcf)
			g_free(element->tst_over_R_tdcf);
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of tst-model (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		element->tst_model_length = length / 2;
		element->tst_model = g_malloc(length * sizeof(double));
		element->tst_over_R_tdcf = g_malloc(length * sizeof(double));
		double_prop = (double *) element->tst_model;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_TST_TF:
		g_mutex_lock(&element->tst_lock);
		if(element->tst_tf) {
			g_free(element->tst_tf);
			element->tst_tf = NULL;
		}
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of tst-tf (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		if(element->tst_tf_length == 0)
			element->tst_tf_length = length / 2;
		else
			g_assert_cmpint(element->tst_tf_length, ==, length / 2);
		element->tst_tf = g_malloc(length * sizeof(double));
		double_prop = (double *) element->tst_tf;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		/* Only apply an update if we have all we need */
		if(element->measured_response && element->pum_tf && element->uim_tf)
			element->need_update = TRUE;
		g_mutex_unlock(&element->tst_lock);
		break;

	case ARG_PUM_FREQS:
		if(element->pum_freqs) {
			g_free(element->pum_freqs);
			element->pum_freqs = NULL;
		}
		if(element->pum_indices) {
			g_free(element->pum_indices);
			element->pum_indices = NULL;
		}
		element->pum_tf_length = element->num_pum_freqs = gst_value_array_get_size(value);
		element->pum_freqs = g_malloc(element->num_pum_freqs * sizeof(double));
		element->pum_indices = g_malloc(element->num_pum_freqs * sizeof(int));
		for(k = 0; k < element->num_pum_freqs; k++)
			element->pum_freqs[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_PUM_SR:
		element->pum_sr = g_value_get_int(value);
		break;

	case ARG_PUM_FIR_LENGTH:
		element->pum_fir_length = g_value_get_int(value);
		element->pumcorr_tf_length = element->pum_fir_length / 2 + 1;
		if(element->pum_window)
			g_free(element->pum_window);
		element->pum_window = blackman_double(element->pum_fir_length, NULL, FALSE);
		if(element->pumcorr_tf)
			g_free(element->pumcorr_tf);
		element->pumcorr_tf = g_malloc(element->pumcorr_tf_length * sizeof(*element->pumcorr_tf));
		break;

	case ARG_FPUM_MODEL:
		if(element->Fpum_model)
			g_free(element->Fpum_model);
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of Fpum-model (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		element->Fpum_model_length = length / 2;
		element->Fpum_model = g_malloc(length * sizeof(double));
		double_prop = (double *) element->Fpum_model;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_PUM_MODEL:
		if(element->pum_model)
			g_free(element->pum_model);
		if(element->pum_over_R_tdcf)
			g_free(element->pum_over_R_tdcf);
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of pum-model (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		element->pum_model_length = length / 2;
		element->pum_model = g_malloc(length * sizeof(double));
		element->pum_over_R_tdcf = g_malloc(length * sizeof(double));
		double_prop = (double *) element->pum_model;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_PUM_TF:
		g_mutex_lock(&element->pum_lock);
		if(element->pum_tf) {
			g_free(element->pum_tf);
			element->pum_tf = NULL;
		}
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of pum-tf (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		if(element->pum_tf_length == 0)
			element->pum_tf_length = length / 2;
		else
			g_assert_cmpint(element->pum_tf_length, ==, length / 2);
		element->pum_tf = g_malloc(length * sizeof(double));
		double_prop = (double *) element->pum_tf;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		/* Only apply an update if we have all we need */
		if(element->measured_response && element->tst_tf && element->uim_tf)
			element->need_update = TRUE;
		g_mutex_unlock(&element->pum_lock);
		break;

	case ARG_UIM_FREQS:
		if(element->uim_freqs) {
			g_free(element->uim_freqs);
			element->uim_freqs = NULL;
		}
		if(element->uim_indices) {
			g_free(element->uim_indices);
			element->uim_indices = NULL;
		}
		element->uim_tf_length = element->num_uim_freqs = gst_value_array_get_size(value);
		element->uim_freqs = g_malloc(element->num_uim_freqs * sizeof(double));
		element->uim_indices = g_malloc(element->num_uim_freqs * sizeof(int));
		for(k = 0; k < element->num_uim_freqs; k++)
			element->uim_freqs[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_UIM_SR:
		element->uim_sr = g_value_get_int(value);
		break;

	case ARG_UIM_FIR_LENGTH:
		element->uim_fir_length = g_value_get_int(value);
		element->uimcorr_tf_length = element->uim_fir_length / 2 + 1;
		if(element->uim_window)
			g_free(element->uim_window);
		element->uim_window = blackman_double(element->uim_fir_length, NULL, FALSE);
		if(element->uimcorr_tf)
			g_free(element->uimcorr_tf);
		element->uimcorr_tf = g_malloc(element->uimcorr_tf_length * sizeof(*element->uimcorr_tf));
		break;

	case ARG_FUIM_MODEL:
		if(element->Fuim_model)
			g_free(element->Fuim_model);
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of Fuim-model (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		element->Fuim_model_length = length / 2;
		element->Fuim_model = g_malloc(length * sizeof(double));
		double_prop = (double *) element->Fuim_model;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_UIM_MODEL:
		if(element->uim_model)
			g_free(element->uim_model);
		if(element->uim_over_R_tdcf)
			g_free(element->uim_over_R_tdcf);
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of uim-model (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		element->uim_model_length = length / 2;
		element->uim_model = g_malloc(length * sizeof(double));
		element->uim_over_R_tdcf = g_malloc(length * sizeof(double));
		double_prop = (double *) element->uim_model;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		break;

	case ARG_UIM_TF:
		g_mutex_lock(&element->uim_lock);
		if(element->uim_tf) {
			g_free(element->uim_tf);
			element->uim_tf = NULL;
		}
		length = gst_value_array_get_size(value);
		if(length % 2) {
			GST_ERROR_OBJECT(element, "Length of uim-tf (%d) must be even, since it is complex.", length);
			g_assert_not_reached();
		}
		if(element->uim_tf_length == 0)
			element->uim_tf_length = length / 2;
		else
			g_assert_cmpint(element->uim_tf_length, ==, length / 2);
		element->uim_tf = g_malloc(length * sizeof(double));
		double_prop = (double *) element->uim_tf;
		for(k = 0; k < length; k++)
			double_prop[k] = g_value_get_double(gst_value_array_get_value(value, k));
		/* Only apply an update if we have all we need */
		if(element->measured_response && element->tst_tf && element->pum_tf)
			element->need_update = TRUE;
		g_mutex_unlock(&element->uim_lock);
		break;

	case ARG_FILENAME:
		element->filename = g_value_dup_string(value);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
		break;
	}

	GST_OBJECT_UNLOCK(element);
}


static void get_property(GObject *object, enum property id, GValue *value, GParamSpec *pspec) {

	GSTLALCalibCorr *element = GSTLAL_CALIBCORR(object);

	int k;
	double *double_prop;

	GST_OBJECT_LOCK(element);

	GValue valuearray = G_VALUE_INIT;

	switch(id) {

	case ARG_CINV_FREQS: ;
		g_value_init(&valuearray, GST_TYPE_ARRAY);
		for(k = 0; k < element->num_cinv_freqs; k++) {
			GValue freq = G_VALUE_INIT;
			g_value_init(&freq, G_TYPE_DOUBLE);
			g_value_set_double(&freq, element->cinv_freqs[k]);
			gst_value_array_append_value(&valuearray, &freq);
			g_value_unset(&freq);
		}
		g_value_copy(&valuearray, value);
		g_value_unset(&valuearray);

		break;

	case ARG_CINV_SR:
		g_value_set_int(value, element->cinv_sr);
		break;

	case ARG_CINV_FIR_LENGTH:
		g_value_set_int(value, element->cinv_fir_length);
		break;

	case ARG_CRES_MODEL:
		if(element->cres_model) {
			double_prop = (double *) element->cres_model;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_cinv_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_MEASURED_RESPONSE:
		if(element->measured_response) {
			double_prop = (double *) element->measured_response;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_cinv_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_CINVCORR_TF:
		if(element->cinvcorr_tf) {
			double_prop = (double *) element->cinvcorr_tf;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->cinvcorr_tf_length; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_CINVCORR_FILT:
		if(element->cinvcorr_filt) {
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < element->cinv_fir_length; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, element->cinvcorr_filt[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_D:
		if(element->D) {
			double_prop = (double *) element->D;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->D_length; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_TST_FREQS: ;
		g_value_init(&valuearray, GST_TYPE_ARRAY);
		for(k = 0; k < element->num_tst_freqs; k++) {
			GValue freq = G_VALUE_INIT;
			g_value_init(&freq, G_TYPE_DOUBLE);
			g_value_set_double(&freq, element->tst_freqs[k]);
			gst_value_array_append_value(&valuearray, &freq);
			g_value_unset(&freq);
		}
		g_value_copy(&valuearray, value);
		g_value_unset(&valuearray);

		break;

	case ARG_TST_SR:
		g_value_set_int(value, element->tst_sr);
		break;

	case ARG_TST_FIR_LENGTH:
		g_value_set_int(value, element->tst_fir_length);
		break;

	case ARG_FTST_MODEL:
		if(element->Ftst_model) {
			double_prop = (double *) element->Ftst_model;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_tst_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_TST_MODEL:
		if(element->tst_model) {
			double_prop = (double *) element->tst_model;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_tst_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_TST_TF:
		if(element->tst_tf) {
			double_prop = (double *) element->tst_tf;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_tst_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_TSTCORR_TF:
		if(element->tstcorr_tf) {
			double_prop = (double *) element->tstcorr_tf;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->tstcorr_tf_length; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_TSTCORR_FILT:
		if(element->tstcorr_filt) {
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < element->tst_fir_length; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, element->tstcorr_filt[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_PUM_FREQS: ;
		g_value_init(&valuearray, GST_TYPE_ARRAY);
		for(k = 0; k < element->num_pum_freqs; k++) {
			GValue freq = G_VALUE_INIT;
			g_value_init(&freq, G_TYPE_DOUBLE);
			g_value_set_double(&freq, element->pum_freqs[k]);
			gst_value_array_append_value(&valuearray, &freq);
			g_value_unset(&freq);
		}
		g_value_copy(&valuearray, value);
		g_value_unset(&valuearray);

		break;

	case ARG_PUM_SR:
		g_value_set_int(value, element->pum_sr);
		break;

	case ARG_PUM_FIR_LENGTH:
		g_value_set_int(value, element->pum_fir_length);
		break;

	case ARG_FPUM_MODEL:
		if(element->Fpum_model) {
			double_prop = (double *) element->Fpum_model;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_pum_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_PUM_MODEL:
		if(element->pum_model) {
			double_prop = (double *) element->pum_model;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_pum_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_PUM_TF:
		if(element->pum_tf) {
			double_prop = (double *) element->pum_tf;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_pum_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_PUMCORR_TF:
		if(element->pumcorr_tf) {
			double_prop = (double *) element->pumcorr_tf;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->pumcorr_tf_length; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_PUMCORR_FILT:
		if(element->pumcorr_filt) {
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < element->pum_fir_length; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, element->pumcorr_filt[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_UIM_FREQS: ;
		g_value_init(&valuearray, GST_TYPE_ARRAY);
		for(k = 0; k < element->num_uim_freqs; k++) {
			GValue freq = G_VALUE_INIT;
			g_value_init(&freq, G_TYPE_DOUBLE);
			g_value_set_double(&freq, element->uim_freqs[k]);
			gst_value_array_append_value(&valuearray, &freq);
			g_value_unset(&freq);
		}
		g_value_copy(&valuearray, value);
		g_value_unset(&valuearray);

		break;

	case ARG_UIM_SR:
		g_value_set_int(value, element->uim_sr);
		break;

	case ARG_UIM_FIR_LENGTH:
		g_value_set_int(value, element->uim_fir_length);
		break;

	case ARG_FUIM_MODEL:
		if(element->Fuim_model) {
			double_prop = (double *) element->Fuim_model;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_uim_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_UIM_MODEL:
		if(element->uim_model) {
			double_prop = (double *) element->uim_model;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_uim_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_UIM_TF:
		if(element->uim_tf) {
			double_prop = (double *) element->uim_tf;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->num_uim_freqs; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_UIMCORR_TF:
		if(element->uimcorr_tf) {
			double_prop = (double *) element->uimcorr_tf;
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < 2 * element->uimcorr_tf_length; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, double_prop[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_UIMCORR_FILT:
		if(element->uimcorr_filt) {
			GValue va = G_VALUE_INIT;
			g_value_init(&va, GST_TYPE_ARRAY);
			for(k = 0; k < element->uim_fir_length; k++) {
				GValue v = G_VALUE_INIT;
				g_value_init(&v, G_TYPE_DOUBLE);
				g_value_set_double(&v, element->uimcorr_filt[k]);
				gst_value_array_append_value(&va, &v);
				g_value_unset(&v);
			}
			g_value_copy(&va, value);
			g_value_unset(&va);
		}
		break;

	case ARG_FILENAME:
		g_value_set_string(value, element->filename);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
		break;
	}

	GST_OBJECT_UNLOCK(element);
}


/*
 * finalize()
 */


static void finalize(GObject *object) {

	GSTLALCalibCorr *element = GSTLAL_CALIBCORR(object);
	if(element->freqs) {
		g_free(element->freqs);
		element->freqs = NULL;
	}
	if(element->R_tdcf) {
		g_free(element->R_tdcf);
		element->R_tdcf = NULL;
	}
	if(element->tst_over_R_tdcf) {
		g_free(element->tst_over_R_tdcf);
		element->tst_over_R_tdcf = NULL;
	}
	if(element->pum_over_R_tdcf) {
		g_free(element->pum_over_R_tdcf);
		element->pum_over_R_tdcf = NULL;
	}
	if(element->uim_over_R_tdcf) {
		g_free(element->uim_over_R_tdcf);
		element->uim_over_R_tdcf = NULL;
	}
	if(element->cinv_freqs) {
		g_free(element->cinv_freqs);
		element->cinv_freqs = NULL;
	}
	if(element->cres_model) {
		g_free(element->cres_model);
		element->cres_model = NULL;
	}
	if(element->measured_response) {
		g_free(element->measured_response);
		element->measured_response = NULL;
	}
	if(element->cinv_indices) {
		g_free(element->cinv_indices);
		element->cinv_indices = NULL;
	}
	if(element->cinv) {
		g_free(element->cinv);
		element->cinv = NULL;
	}
	if(element->cinvcorr_tf) {
		g_free(element->cinvcorr_tf);
		element->cinvcorr_tf = NULL;
	}
	if(element->cinvcorr_filt) {
		g_free(element->cinvcorr_filt);
		element->cinvcorr_filt = NULL;
	}
	if(element->D) {
		g_free(element->D);
		element->D = NULL;
	}
	if(element->tst_freqs) {
		g_free(element->tst_freqs);
		element->tst_freqs = NULL;
	}
	if(element->Ftst_model) {
		g_free(element->Ftst_model);
		element->Ftst_model = NULL;
	}
	if(element->tst_model) {
		g_free(element->tst_model);
		element->tst_model = NULL;
	}
	if(element->tst_tf) {
		g_free(element->tst_tf);
		element->tst_tf = NULL;
	}
	if(element->tst_indices) {
		g_free(element->tst_indices);
		element->tst_indices = NULL;
	}
	if(element->tst) {
		g_free(element->tst);
		element->tst = NULL;
	}
	if(element->tstcorr_tf) {
		g_free(element->tstcorr_tf);
		element->tstcorr_tf = NULL;
	}
	if(element->tstcorr_filt) {
		g_free(element->tstcorr_filt);
		element->tstcorr_filt = NULL;
	}
	if(element->pum_freqs) {
		g_free(element->pum_freqs);
		element->pum_freqs = NULL;
	}
	if(element->Fpum_model) {
		g_free(element->Fpum_model);
		element->Fpum_model = NULL;
	}
	if(element->pum_model) {
		g_free(element->pum_model);
		element->pum_model = NULL;
	}
	if(element->pum_tf) {
		g_free(element->pum_tf);
		element->pum_tf = NULL;
	}
	if(element->pum_indices) {
		g_free(element->pum_indices);
		element->pum_indices = NULL;
	}
	if(element->pum) {
		g_free(element->pum);
		element->pum = NULL;
	}
	if(element->pumcorr_tf) {
		g_free(element->pumcorr_tf);
		element->pumcorr_tf = NULL;
	}
	if(element->pumcorr_filt) {
		g_free(element->pumcorr_filt);
		element->pumcorr_filt = NULL;
	}
	if(element->uim_freqs) {
		g_free(element->uim_freqs);
		element->uim_freqs = NULL;
	}
	if(element->Fuim_model) {
		g_free(element->Fuim_model);
		element->Fuim_model = NULL;
	}
	if(element->uim_model) {
		g_free(element->uim_model);
		element->uim_model = NULL;
	}
	if(element->uim_tf) {
		g_free(element->uim_tf);
		element->uim_tf = NULL;
	}
	if(element->uim_indices) {
		g_free(element->uim_indices);
		element->uim_indices = NULL;
	}
	if(element->uim) {
		g_free(element->uim);
		element->uim = NULL;
	}
	if(element->uimcorr_tf) {
		g_free(element->uimcorr_tf);
		element->uimcorr_tf = NULL;
	}
	if(element->uimcorr_filt) {
		g_free(element->uimcorr_filt);
		element->uimcorr_filt = NULL;
	}
	if(element->filename) {
		g_free(element->filename);
		element->filename = NULL;
	}

	G_OBJECT_CLASS(gstlal_calibcorr_parent_class)->finalize(object);
}


/*
 * class_init()
 */


#define CAPS \
	"audio/x-raw, " \
	"rate = (int) [1, MAX], " \
	"channels = (int) 10, " \
	"format = (string) {"GST_AUDIO_NE(F64)"}, " \
	"layout = (string) interleaved, " \
	"channel-mask = (bitmask) 0"


static void gstlal_calibcorr_class_init(GSTLALCalibCorrClass *klass) {

	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
	GstElementClass *element_class = GST_ELEMENT_CLASS(klass);
	GstBaseSinkClass *gstbasesink_class = GST_BASE_SINK_CLASS(klass);

	gstbasesink_class->start = GST_DEBUG_FUNCPTR(start);
	gstbasesink_class->set_caps = GST_DEBUG_FUNCPTR(set_caps);
	gstbasesink_class->render = GST_DEBUG_FUNCPTR(render);

	gobject_class->set_property = GST_DEBUG_FUNCPTR(set_property);
	gobject_class->get_property = GST_DEBUG_FUNCPTR(get_property);
	gobject_class->finalize = GST_DEBUG_FUNCPTR(finalize);

	gst_element_class_set_details_simple(
		element_class,
		"Correct Calibration Error",
		"Sink",
		"Provided static models of the calibration components, the TDCFs as an interleaved\n\t\t\t   "
		"input stream, and transfer functions computed from injections made using the Pcal and\n\t\t\t   "
		"all three stages of actuation, this element computes FIR filters for each component of\n\t\t\t   "
		"the calibration (C^{-1}, TST, PUM, UIM) to compensate for any remaining systematic\n\t\t\t   "
		"error.  The static models and transfer functions need to be evaluated only at a select\n\t\t\t   "
		"comb of frequencies.  Input channels are the TDCFs, in this order: k_tst, tau_tst,\n\t\t\t   "
		"k_pum, tau_pum, k_uim, tau_uim, k_C, f_cc, f_s^2, f_s / Q.",
		"Aaron Viets <aaron.viets@ligo.org>"
	);

	gst_element_class_add_pad_template(
		element_class,
		gst_pad_template_new(
			"sink",
			GST_PAD_SINK,
			GST_PAD_ALWAYS,
			gst_caps_from_string(CAPS)
		)
	);


	properties[ARG_CINV_FREQS] = gst_param_spec_array(
		"cinv-freqs",
		"Inverse Sensing Frequencies",
		"Array of frequencies in Hz at which measurements are made using Pcal",
		g_param_spec_double(
			"frequency",
			"Frequency",
			"A frequency in the array",
			0.0, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_CINV_SR] = g_param_spec_int(
		"cinv-sr",
		"Inverse Sensing Sample Rate",
		"Sample rate of the FIR filter produced for the inverse sensing path",
		1, G_MAXINT, 16384,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_CINV_FIR_LENGTH] = g_param_spec_int(
		"cinv-fir-length",
		"Inverse Sensing FIR Length",
		"Length in samples of the FIR filter produced for the inverse sensing path",
		1, G_MAXINT, 1024,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_CRES_MODEL] = gst_param_spec_array(
		"cres-model",
		"Time-independent Portion of Sensing Model",
		"Frequency-domain model of the sensing function, not including the time-dependent\n\t\t\t"
		"portion, sampled at all measurement frequencies plus DC and the highest Nyquist\n\t\t\t"
		"component of all paths.  Even elements of the array\n\t\t\t"
		"represent real parts, and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_MEASURED_RESPONSE] = gst_param_spec_array(
		"measured-response",
		"Measured Response Function",
		"Frequency-domain response function measured as Pcal(f) / d_err(f), sampled\n\t\t\t"
		"at the frequencies cinv-freqs.  Even elements of the array represent real parts,\n\t\t\t"
		"and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_CINVCORR_TF] = gst_param_spec_array(
		"cinvcorr-tf",
		"Inverse Sensing Correction Transfer Function",
		"Computed correction to the inverse sensing path, as a frequency-domain transfer\n\t\t\t"
		"function, evenly sampled from DC to the Nyquist rate of the inverse sensing\n\t\t\t"
		"filter.  Even elements of the array represent real parts, and odd elements\n\t\t\t"
		"represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the transfer function at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_CINVCORR_FILT] = gst_param_spec_array(
		"cinvcorr-filt",
		"Inverse Sensing Correction FIR Filter",
		"The FIR filter used to apply corrections to the inverse sensing path",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the FIR filter at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_D] = gst_param_spec_array(
		"D",
		"Digital Filter Model",
		"Frequency-domain model of the digital filter D, sampled at all\n\t\t\t"
		"measurement frequencies.  Even elements of the array represent real parts,\n\t\t\t"
		"and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_TST_FREQS] = gst_param_spec_array(
		"tst-freqs",
		"TST Frequencies",
		"Array of frequencies in Hz at which measurements are made using TST driver",
		g_param_spec_double(
			"frequency",
			"Frequency",
			"A frequency in the array",
			0.0, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_TST_SR] = g_param_spec_int(
		"tst-sr",
		"TST Sample Rate",
		"Sample rate of the FIR filter produced for the TST path",
		1, G_MAXINT, 2048,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_TST_FIR_LENGTH] = g_param_spec_int(
		"tst-fir-length",
		"TST FIR Length",
		"Length in samples of the FIR filter produced for the TST path",
		1, G_MAXINT, 128,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_FTST_MODEL] = gst_param_spec_array(
		"Ftst-model",
		"F_TST Model",
		"Frequency-domain model of all that comes before the injection point in\n\t\t\t"
		"A_TST, sampled at all measurement frequencies plus DC and the highest\n\t\t\t"
		"Nyquist component of all paths.  Even elements of the\n\t\t\t"
		"array represent real parts, and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_TST_MODEL] = gst_param_spec_array(
		"tst-model",
		"TST Model",
		"Frequency-domain model of A_TST, sampled at all measurement frequencies\n\t\t\t"
		"plus DC and the highest Nyquist component of all paths.\n\t\t\t"
		"Includes only the portion of A_TST after the injection point.\n\t\t\t"
		"Even elements of the array represent real parts,\n\t\t\t"
		"and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_TST_TF] = gst_param_spec_array(
		"tst-tf",
		"Measured TST Transfer Function",
		"Frequency-domain transfer function measured as d_err(f) / x_TST(f), sampled\n\t\t\t"
		"at the frequencies tst-freqs.  Even elements of the array represent real parts,\n\t\t\t"
		"and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_TSTCORR_TF] = gst_param_spec_array(
		"tstcorr-tf",
		"TST Correction Transfer Function",
		"Computed correction to the TST path, as a frequency-domain transfer\n\t\t\t"
		"function, evenly sampled from DC to the Nyquist rate of the TST\n\t\t\t"
		"filter.  Even elements of the array represent real parts, and odd elements\n\t\t\t"
		"represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the transfer function at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_TSTCORR_FILT] = gst_param_spec_array(
		"tstcorr-filt",
		"TST Correction FIR Filter",
		"The FIR filter used to apply corrections to the TST path",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the FIR filter at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_PUM_FREQS] = gst_param_spec_array(
		"pum-freqs",
		"PUM Frequencies",
		"Array of frequencies in Hz at which measurements are made using PUM driver",
		g_param_spec_double(
			"frequency",
			"Frequency",
			"A frequency in the array",
			0.0, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_PUM_SR] = g_param_spec_int(
		"pum-sr",
		"PUM Sample Rate",
		"Sample rate of the FIR filter produced for the PUM path",
		1, G_MAXINT, 2048,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_PUM_FIR_LENGTH] = g_param_spec_int(
		"pum-fir-length",
		"PUM FIR Length",
		"Length in samples of the FIR filter produced for the PUM path",
		1, G_MAXINT, 128,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_FPUM_MODEL] = gst_param_spec_array(
		"Fpum-model",
		"F_PUM Model",
		"Frequency-domain model of all that comes before the injection point in\n\t\t\t"
		"A_PUM, sampled at all measurement frequencies plus DC and the highest\n\t\t\t"
		"Nyquist component of all paths.  Even elements of the\n\t\t\t"
		"array represent real parts, and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_PUM_MODEL] = gst_param_spec_array(
		"pum-model",
		"PUM Model",
		"Frequency-domain model of A_PUM, sampled at all measurement frequencies\n\t\t\t"
		"plus DC and the highest Nyquist component of all paths.\n\t\t\t"
		"Includes only the portion of A_PUM after the injection point.\n\t\t\t"
		"Even elements of the array represent real parts,\n\t\t\t"
		"and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_PUM_TF] = gst_param_spec_array(
		"pum-tf",
		"Measured PUM Transfer Function",
		"Frequency-domain transfer function measured as d_err(f) / x_PUM(f), sampled\n\t\t\t"
		"at the frequencies pum-freqs.  Even elements of the array represent real parts,\n\t\t\t"
		"and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_PUMCORR_TF] = gst_param_spec_array(
		"pumcorr-tf",
		"PUM Correction Transfer Function",
		"Computed correction to the PUM path, as a frequency-domain transfer\n\t\t\t"
		"function, evenly sampled from DC to the Nyquist rate of the PUM\n\t\t\t"
		"filter.  Even elements of the array represent real parts, and odd elements\n\t\t\t"
		"represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the transfer function at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_PUMCORR_FILT] = gst_param_spec_array(
		"pumcorr-filt",
		"PUM Correction FIR Filter",
		"The FIR filter used to apply corrections to the PUM path",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the FIR filter at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	 properties[ARG_UIM_FREQS] = gst_param_spec_array(
		"uim-freqs",
		"UIM Frequencies",
		"Array of frequencies in Hz at which measurements are made using UIM driver",
		g_param_spec_double(
			"frequency",
			"Frequency",
			"A frequency in the array",
			0.0, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_UIM_SR] = g_param_spec_int(
		"uim-sr",
		"UIM Sample Rate",
		"Sample rate of the FIR filter produced for the UIM path",
		1, G_MAXINT, 2048,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_UIM_FIR_LENGTH] = g_param_spec_int(
		"uim-fir-length",
		"UIM FIR Length",
		"Length in samples of the FIR filter produced for the UIM path",
		1, G_MAXINT, 128,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_FUIM_MODEL] = gst_param_spec_array(
		"Fuim-model",
		"F_UIM Model",
		"Frequency-domain model of all that comes before the injection point in\n\t\t\t"
		"A_UIM, sampled at all measurement frequencies plus DC and the highest\n\t\t\t"
		"Nyquist component of all paths.  Even elements of the\n\t\t\t"
		"array represent real parts, and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_UIM_MODEL] = gst_param_spec_array(
		"uim-model",
		"UIM Model",
		"Frequency-domain model of A_UIM, sampled at all measurement frequencies\n\t\t\t"
		"plus DC and the highest Nyquist component of all paths.\n\t\t\t"
		"Includes only the portion of A_UIM after the injection point.\n\t\t\t"
		"Even elements of the array represent real parts,\n\t\t\t"
		"and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_UIM_TF] = gst_param_spec_array(
		"uim-tf",
		"Measured UIM Transfer Function",
		"Frequency-domain transfer function measured as d_err(f) / x_UIM(f), sampled\n\t\t\t"
		"at the frequencies uim-freqs.  Even elements of the array represent real parts,\n\t\t\t"
		"and odd elements represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the model at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_UIMCORR_TF] = gst_param_spec_array(
		"uimcorr-tf",
		"UIM Correction Transfer Function",
		"Computed correction to the UIM path, as a frequency-domain transfer\n\t\t\t"
		"function, evenly sampled from DC to the Nyquist rate of the UIM\n\t\t\t"
		"filter.  Even elements of the array represent real parts, and odd elements\n\t\t\t"
		"represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the transfer function at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_UIMCORR_FILT] = gst_param_spec_array(
		"uimcorr-filt",
		"UIM Correction FIR Filter",
		"The FIR filter used to apply corrections to the UIM path",
		g_param_spec_double(
			"value",
			"Value",
			"Value of the FIR filter at a particular frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_FILENAME] = g_param_spec_string(
		"filename",
		"Filename",
		"Name of file in which to write the computed corrections.  If not given, no\n\t\t\t"
		"file is produced.",
		NULL,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);


	g_object_class_install_property(
		gobject_class,
		ARG_CINV_FREQS,
		properties[ARG_CINV_FREQS]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_CINV_SR,
		properties[ARG_CINV_SR]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_CINV_FIR_LENGTH,
		properties[ARG_CINV_FIR_LENGTH]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_CRES_MODEL,
		properties[ARG_CRES_MODEL]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_MEASURED_RESPONSE,
		properties[ARG_MEASURED_RESPONSE]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_CINVCORR_TF,
		properties[ARG_CINVCORR_TF]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_CINVCORR_FILT,
		properties[ARG_CINVCORR_FILT]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_D,
		properties[ARG_D]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_TST_FREQS,
		properties[ARG_TST_FREQS]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_TST_SR,
		properties[ARG_TST_SR]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_TST_FIR_LENGTH,
		properties[ARG_TST_FIR_LENGTH]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_FTST_MODEL,
		properties[ARG_FTST_MODEL]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_TST_MODEL,
		properties[ARG_TST_MODEL]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_TST_TF,
		properties[ARG_TST_TF]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_TSTCORR_TF,
		properties[ARG_TSTCORR_TF]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_TSTCORR_FILT,
		properties[ARG_TSTCORR_FILT]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_PUM_FREQS,
		properties[ARG_PUM_FREQS]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_PUM_SR,
		properties[ARG_PUM_SR]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_PUM_FIR_LENGTH,
		properties[ARG_PUM_FIR_LENGTH]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_FPUM_MODEL,
		properties[ARG_FPUM_MODEL]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_PUM_MODEL,
		properties[ARG_PUM_MODEL]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_PUM_TF,
		properties[ARG_PUM_TF]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_PUMCORR_TF,
		properties[ARG_PUMCORR_TF]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_PUMCORR_FILT,
		properties[ARG_PUMCORR_FILT]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_UIM_FREQS,
		properties[ARG_UIM_FREQS]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_UIM_SR,
		properties[ARG_UIM_SR]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_UIM_FIR_LENGTH,
		properties[ARG_UIM_FIR_LENGTH]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_FUIM_MODEL,
		properties[ARG_FUIM_MODEL]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_UIM_MODEL,
		properties[ARG_UIM_MODEL]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_UIM_TF,
		properties[ARG_UIM_TF]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_UIMCORR_TF,
		properties[ARG_UIMCORR_TF]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_UIMCORR_FILT,
		properties[ARG_UIMCORR_FILT]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_FILENAME,
		properties[ARG_FILENAME]
	);
}


/*
 * init()
 */


static void gstlal_calibcorr_init(GSTLALCalibCorr *element) {

	g_signal_connect(G_OBJECT(element), "notify::cinvcorr-tf", G_CALLBACK(rebuild_workspace_and_reset), NULL);
	g_signal_connect(G_OBJECT(element), "notify::cinvcorr-filt", G_CALLBACK(rebuild_workspace_and_reset), NULL);
	g_signal_connect(G_OBJECT(element), "notify::tstcorr-tf", G_CALLBACK(rebuild_workspace_and_reset), NULL);
	g_signal_connect(G_OBJECT(element), "notify::tstcorr-filt", G_CALLBACK(rebuild_workspace_and_reset), NULL);
	g_signal_connect(G_OBJECT(element), "notify::pumcorr-tf", G_CALLBACK(rebuild_workspace_and_reset), NULL);
	g_signal_connect(G_OBJECT(element), "notify::pumcorr-filt", G_CALLBACK(rebuild_workspace_and_reset), NULL);
	g_signal_connect(G_OBJECT(element), "notify::uimcorr-tf", G_CALLBACK(rebuild_workspace_and_reset), NULL);
	g_signal_connect(G_OBJECT(element), "notify::uimcorr-filt", G_CALLBACK(rebuild_workspace_and_reset), NULL);

	element->need_update = FALSE;

	element->freqs = NULL;
	element->num_freqs = 0;

	element->R_tdcf = NULL;
	element->tst_over_R_tdcf = NULL;
	element->pum_over_R_tdcf = NULL;
	element->uim_over_R_tdcf = NULL;

	element->cinv_freqs = NULL;
	element->num_cinv_freqs = 0;
	element->cres_model = NULL;
	element->cres_model_length = 0;
	element->measured_response = NULL;
	element->cinv_indices = NULL;
	element->measured_response_length = 0;
	element->cinv = NULL;
	element->cinvcorr_tf = NULL;
	element->cinvcorr_filt = NULL;

	element->D = NULL;
	element->D_length = 0;

	element->tst_freqs = NULL;
	element->num_tst_freqs = 0;
	element->Ftst_model = NULL;
	element->Ftst_model_length = 0;
	element->tst_model = NULL;
	element->tst_model_length = 0;
	element->tst_tf = NULL;
	element->tst_indices = NULL;
	element->tst_tf_length = 0;
	element->tst = NULL;
	element->tstcorr_tf = NULL;
	element->tstcorr_filt = NULL;

	element->pum_freqs = NULL;
	element->num_pum_freqs = 0;
	element->Fpum_model = NULL;
	element->Fpum_model_length = 0;
	element->pum_model = NULL;
	element->pum_model_length = 0;
	element->pum_tf = NULL;
	element->pum_indices = NULL;
	element->pum_tf_length = 0;
	element->pum = NULL;
	element->pumcorr_tf = NULL;
	element->pumcorr_filt = NULL;

	element->uim_freqs = NULL;
	element->num_uim_freqs = 0;
	element->Fuim_model = NULL;
	element->Fuim_model_length = 0;
	element->uim_model = NULL;
	element->uim_model_length = 0;
	element->uim_tf = NULL;
	element->uim_indices = NULL;
	element->uim_tf_length = 0;
	element->uim = NULL;
	element->uimcorr_tf = NULL;
	element->uimcorr_filt = NULL;

	element->filename = NULL;

	element->rate = 0;
	element->unit_size = 0;
	element->channels = 0;

	gst_base_sink_set_sync(GST_BASE_SINK(element), FALSE);
	gst_base_sink_set_async_enabled(GST_BASE_SINK(element), FALSE);
}


