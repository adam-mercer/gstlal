#!/usr/bin/env python3
#
# Copyright (C) 2010  Kipp Cannon, Chad Hanna, Leo Singer
# Copyright (C) 2009  Kipp Cannon, Chad Hanna
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""Build time-sliced, SVD'd filter banks for use with gstlal_inspiral"""


#
#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#


from optparse import OptionParser
import numpy
import warnings

import lal.series
from lal.utils import CacheEntry
from ligo.lw import utils as ligolw_utils
from gstlal import svd_bank
from gstlal.stats import inspiral_lr


warnings.warn(
	"this program has been replaced by gstlal_inspiral_svd_bank, "
	"and will be removed from gstlal in a future release",
	DeprecationWarning,
)

### This program will create svd bank files; see gstlal_svd_bank for more information
###
### Usage examples
### --------------
###
### 1. Typical use::
###
### 	gstlal_svd_bank --reference-psd reference_psd.xml --samples-min 1024 \
###	--bank-id 0 --ortho-gate-fap 0.5 --flow 40.0 \
###	--template-bank /mnt/qfs3/gstlalcbc/engineering/5/bns_bank_40Hz/0000-H1_split_bank-H1-TMPLTBANK-871147516-2048.xml \
###	--svd-tolerance 0.9995
###	--write-svd-bank /mnt/qfs3/gstlalcbc/engineering/5/bns_bank_40Hz/svd_0000-H1_split_bank-H1-TMPLTBANK-871147516-2048.xml \
###	--samples-max-64 4096 --clipleft 0 --autocorrelation-length 351 --samples-max-256 1024 --clipright 20 --samples-max 4096
###
### 2. Please add more!
###
### Review Status
### -------------
###
### +-------------------------------------------------+------------------------------------------+------------+
### | Names                                           | Hash                                     | Date       |
### +=================================================+==========================================+============+
### | Florent, Sathya, Duncan Me., Jolien, Kipp, Chad | 7536db9d496be9a014559f4e273e1e856047bf71 | 2014-04-30 |
### +-------------------------------------------------+------------------------------------------+------------+
###


#
#
# =============================================================================
#
#                                 Command Line
#
# =============================================================================
#


parser = OptionParser(description = __doc__)
parser.add_option("--flow", metavar = "Hz", type = "float", default = 40.0, help = "Set the template low-frequency cut-off (default = 40.0).")
parser.add_option("--sample-rate", metavar = "Hz", type = "int", help = "Set the sample rate.  If not set, the sample rate will be based on the template frequency.  The sample rate must be at least twice the highest frequency in the templates. If provided it must be a power of two")
parser.add_option("--identity-transform", action = "store_true", default = False, help = "Do not perform an SVD; instead, use the original templates as the analyzing templates.")
parser.add_option("--append-time-reversed-template", action = "store_true", help = "A shortcut for appending time reversed template bank to the output file without manually adding --bank-type. (optional; cannot combined with --bank-type.)")
parser.add_option("--bank-type", type= "string",  metavar = "N", action = "append", default = [], help = "Define the type of the template bank: is it used to produce signal candidates or it is used to produce noise candidate? Use 'noise_model' to indicate that it's for noise candidates or 'signal_model' to indicate for signal candidates (default). (optional; if provided, it must be as many as --template-bank).")
parser.add_option("--padding", metavar = "pad", type = "float", default = 1.5, help = "Fractional amount to pad time slices.")
parser.add_option("--svd-tolerance", metavar = "match", type = "float", default = 0.9995, help = "Set the SVD reconstruction tolerance (default = 0.9995).")
parser.add_option("--reference-psd", metavar = "filename", help = "Load the spectrum from this LIGO light-weight XML file (required).")
parser.add_option("--template-bank", metavar = "filename", action = "append", default = [], help = "Set the name of the LIGO light-weight XML file from which to load the template bank (required).")
parser.add_option("--template-bank-cache", metavar = "filename", help = "Provide a cache file with the names of the LIGO light-weight XML file from which to load the template bank.")
parser.add_option("--ortho-gate-fap", metavar = "probability", type = "float", default = 0.5, help = "Set the orthogonal SNR projection gate false-alarm probability (default = 0.5).")
parser.add_option("--write-svd-bank", metavar = "filename", help = "Set the filename in which to save the template bank (required).")
parser.add_option("-v", "--verbose", action = "store_true", help = "Be verbose (optional).")
parser.add_option("--clipleft", type = "int", metavar = "N", action = "append", help = "Remove N poorly reconstructable templates from the left edge of each sub-bank. Required")
parser.add_option("--clipright", type = "int", metavar = "N", action = "append", help = "Remove N poorly reconstructable templates from the right edge of each sub-bank. Required")
parser.add_option("--autocorrelation-length", type = "int", default = 201, help = "The minimum number of samples to use for auto-chisquared, default 201 should be odd")
parser.add_option("--samples-min", type = "int", default = 1024, help = "The minimum number of samples to use for time slices default 1024")
parser.add_option("--samples-max-256", type = "int", default = 1024, help = "The maximum number of samples to use for time slices with frequencies above 256Hz, default 1024")
parser.add_option("--samples-max-64", type = "int", default = 2048, help = "The maximum number of samples to use for time slices with frequencies between 64Hz and 256 Hz, default 2048")
parser.add_option("--samples-max", type = "int", default = 4096, help = "The maximum number of samples to use for time slices with frequencies below 64Hz, default 4096")
parser.add_option("--bank-id", metavar = "id", action = "append", help = "Set a string to be used as the globally unique ID for each bank, required.  IDs should be of the form <int>ID_<int>N where N is for the sub bank in each file and this option must be given N times")

options, filenames = parser.parse_args()

if options.template_bank_cache:
	options.template_bank.extend([CacheEntry(line).url for line in open(options.template_bank_cache)])

if len(options.bank_type) > 0 and options.append_time_reversed_template:
	raise ValueError("--bank-type and --append-time-reversed-template cannot be used together")

if options.append_time_reversed_template:
	options.clipleft += options.clipleft
	options.clipright += options.clipright
	ID, N = options.bank_id[-1].split("_")
	options.bank_id += [ID + "_" + str(i) for i in range(int(N)+1 , int(N) + len(options.template_bank) + 1)]
	options.bank_type = ["signal_model"] * len(options.template_bank) + ["noise_model"] * len(options.template_bank)
	options.template_bank += options.template_bank

if len(options.bank_type) == 0:
	options.bank_type = ["signal_model"] * len(options.template_bank)

required_options = ("reference_psd", "template_bank", "write_svd_bank", "clipleft", "clipright", "bank_id")

missing_options = [option for option in required_options if getattr(options, option) is None]
if missing_options:
	raise ValueError("missing required option(s) %s" % ", ".join("--%s" % option.replace("_", "-") for option in sorted(missing_options)))

if not (len(options.template_bank) == len(options.clipleft) == len(options.clipright) == len(options.bank_id) == len(options.bank_type)):
	raise ValueError("must give --template-bank, --bank-type, --clipright and --clipleft options in equal amounts")

if any([bank_type not in ("signal_model", "noise_model") for bank_type in options.bank_type]):
	raise ValueError("--bank-type must be either 'signal_model' or 'noise_model'")

if not options.autocorrelation_length % 2:
	raise ValueError("--autocorrelation-length must be odd")

if options.sample_rate is not None and (not numpy.log2(options.sample_rate) == int(numpy.log2(options.sample_rate))):
	raise ValueError("--sample-rate must be a power of two")



#
#
# =============================================================================
#
#                                     Main
#
# =============================================================================
#


psd = lal.series.read_psd_xmldoc(ligolw_utils.load_filename(options.reference_psd, verbose=options.verbose, contenthandler=lal.series.PSDContentHandler))

svd_bank.write_bank(
	options.write_svd_bank,
	[svd_bank.build_bank(
		template_bank,
		psd,
		options.flow,
		options.ortho_gate_fap,
		inspiral_lr.LnLRDensity.snr_min,
		options.svd_tolerance,
		clipleft,
		clipright,
		padding = options.padding,
		identity_transform = options.identity_transform,
		bank_type = bank_type,
		verbose = options.verbose,
		autocorrelation_length = options.autocorrelation_length,
		samples_min = options.samples_min,
		samples_max_256 = options.samples_max_256,
		samples_max_64 = options.samples_max_64,
		samples_max = options.samples_max,
		bank_id = bank_id,
		contenthandler = svd_bank.DefaultContentHandler,
		sample_rate = options.sample_rate
	) for (template_bank, bank_id, clipleft, clipright, bank_type) in zip(options.template_bank, options.bank_id, options.clipleft, options.clipright, options.bank_type)],
	psd
)
