#!/usr/bin/env python3
#
# Copyright (C) 2020  Patrick Godwin (patrick.godwin@ligo.org)
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import argparse

from gstlal.config.inspiral import Config
from gstlal.dags.inspiral import DAG
from gstlal.datafind import DataCache, DataType


parser = argparse.ArgumentParser()
parser.add_argument("-c", "--config", help="Sets the path to read configuration from.")
parser.add_argument("-w", "--workflow", default="inspiral", help="Sets the type of workflow to run.")

# load config
args = parser.parse_args()
config = Config.load(args.config)
config.setup()

# create dag
dag = DAG(config)
dag.create_log_dir()

if args.workflow == "setup":
	# input data products
	ref_psd = DataCache.from_files(DataType.REFERENCE_PSD, config.data.reference_psd)
	split_bank = DataCache.find(DataType.SPLIT_BANK, svd_bins="*", subtype="*")

	# generate dag layers
	svd_bank = dag.svd_bank(ref_psd, split_bank)
	if config.svd.checkerboard:
		svd_bank = dag.checkerboard(ref_psd, svd_bank)
	dist_stats = dag.create_prior(svd_bank, ref_psd)

else:
	# input data products
	svd_banks = DataCache.find(DataType.SVD_BANK, root="filter", svd_bins="*")
	dist_stats = DataCache.find(DataType.DIST_STATS, svd_bins="*")

	zerolag_pdfs = DataCache.generate(
		DataType.ZEROLAG_DIST_STAT_PDFS,
		config.all_ifos,
		svd_bins=config.svd.bins,
	)
	marg_pdf = DataCache.generate(DataType.DIST_STAT_PDFS, config.all_ifos)

	# generate dag layers
	if config.filter.injections:
		dag.filter_injections_online(svd_banks, dist_stats, zerolag_pdfs, marg_pdf)
	dag.filter_online(svd_banks, dist_stats, zerolag_pdfs, marg_pdf)
	dag.marginalize_online(marg_pdf)
	dag.track_noise()
	if config.services.kafka_server:
		dag.count_events(dist_stats)
		dag.upload_events()
		dag.plot_events()
		dag.collect_metrics()

# write dag/script to disk
dag_name = f"online_{args.workflow}_dag"
dag.write_dag(f"{dag_name}.dag")
dag.write_script(f"{dag_name}.sh")
