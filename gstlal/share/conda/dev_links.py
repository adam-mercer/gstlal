"""Script for creating developer links - mostly for IDE support"""


import pathlib
import os


CONDA_ROOT = pathlib.Path(__file__).parent
REPO_ROOT = CONDA_ROOT.parent.parent.parent
DEV_LINKS_DIR_NAME = 'devlinks'
DEV_LINKS_ROOT = REPO_ROOT / DEV_LINKS_DIR_NAME / 'gstlal'
GSTLAL_ROOT = REPO_ROOT / 'gstlal'
GSTLAL_UGLY_ROOT = REPO_ROOT / 'gstlal-ugly'
GSTLAL_BURST_ROOT = REPO_ROOT / 'gstlal-burst'
GSTLAL_CALIBRATION_ROOT = REPO_ROOT / 'gstlal-calibration'
GSTLAL_INSPIRAL_ROOT = REPO_ROOT / 'gstlal-inspiral'
PYTHON_ROOTS = [r / 'python' for r in [
	GSTLAL_ROOT,
	GSTLAL_UGLY_ROOT,
	GSTLAL_BURST_ROOT,
	GSTLAL_CALIBRATION_ROOT,
	GSTLAL_INSPIRAL_ROOT,
]]
FILE_SUFFIXES = ['py', 'la']

def get_python_files(dir: pathlib.Path, suffix: str):
	return dir.rglob('*.{}'.format(suffix))


def make_mimic_links(files: list, src_root: pathlib.Path, dest_root: pathlib.Path):
	if not dest_root.exists():
		dest_root.mkdir(parents=True, exist_ok=True)

	for f in files:
		relative_path = f.relative_to(src_root)
		dest_path = dest_root / relative_path

		if f.parent != src_root: # have subdirs
			dest_path.parent.mkdir(parents=True, exist_ok=True)

		if not dest_path.exists():
			os.symlink(f.as_posix(), dest_path.as_posix())


def main():
	for r in PYTHON_ROOTS:
		for s in FILE_SUFFIXES:
			files = get_python_files(r, s)
			make_mimic_links(files, r, DEV_LINKS_ROOT)


if __name__ == '__main__':
	main()


