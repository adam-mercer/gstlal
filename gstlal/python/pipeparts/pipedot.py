"""Utilities for converting Gst pipelines into DOT graphs
"""
import os
import pathlib
import sys

import gi

from gstlal.pipeparts import pipetools

gi.require_version('Gst', '1.0')
from gi.repository import GObject
from gi.repository import Gst

GObject.threads_init()
Gst.init(None)

ENV_VAR_DUMP_DIR = 'GST_DEBUG_DUMP_DOT_DIR'


def to_file(pipeline: pipetools.Pipeline, filestem: str, verbose: bool = False, use_str_method: bool = False):
	"""Write a pipeline out to a dot file. This function needs the environment variable GST_DEBUG_DUMP_DOT_DIR
	to be set.

	Args:
		pipeline:
		filestem:
			str, name of file (not including extension)
		verbose:
			bool, default False, if True a message will be written to stderr.
		use_str_method:
			bool, default False, if True use the "to_str" method as an intermediate step. This is enabled due to
			some bugs in Gst where no files are written out using the direct "debug_bin_to_dot_file".

	Notes:
		File Output:
			The filename will be os.path.join($GST_DEBUG_DUMP_DOT_DIR, filestem + ".dot")

	Raises:
		ValueError:
			If "GST_DEBUG_DUMP_DOT_DIR" env var not defined

	References:
		[1] https://lazka.github.io/pgi-docs/Gst-1.0/functions.html#Gst.debug_bin_to_dot_file

	Returns:
		None
	"""
	if ENV_VAR_DUMP_DIR not in os.environ:
		raise ValueError("cannot write pipeline, environment variable GST_DEBUG_DUMP_DOT_DIR is not set")

	output_path = pathlib.Path(os.environ[ENV_VAR_DUMP_DIR]) / '{}.dot'.format(filestem)

	if use_str_method:
		dot_str = to_str(pipeline)
		with open(output_path.as_posix(), 'w') as fid:
			fid.write(dot_str)
	else:
		Gst.debug_bin_to_dot_file(pipeline, Gst.DebugGraphDetails.ALL, filestem)
	if verbose:
		print("Wrote pipeline to {}".format(output_path.as_posix()), file=sys.stderr)


def to_str(pipeline: pipetools.Pipeline) -> str:
	"""Convert a pipeline to a DOT-formatted string directly (no out file intermediary)

	Args:
		pipeline:
			Pipeline, the pipeline to convert to DOT string

	References:
		[1] https://gstreamer.freedesktop.org/documentation/gstreamer/debugutils.html?gi-language=python

	Returns:
		str, the pipeline converted to DOT formatted string
	"""
	return Gst.debug_bin_to_dot_data(pipeline, Gst.DebugGraphDetails.ALL)
