#!/usr/bin/env python3
#
# Copyright (C) 2011  Chad Hanna
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from optparse import OptionParser

from gstlal.psd import read_asd_txt, write_psd

### A program to turn an ASCII PSD or ASD into an xml file suitable for
### consumption by various gstlal programs
###
###
### Usage
### -----
###
### 1. An early V1 input ASD from <a href=http://www.lsc-group.phys.uwm.edu/cgit/gstlal/plain/gstlal/share/v1_early_asd.txt>here</a>.
###
### .. code-block:: bash
###
###	gstlal_psd_xml_from_asd_txt --instrument=V1 --output v1psd.xml.gz v1_early_asd.txt
###
###
### 2. Other things possible, please add some!
###
###
### Notes
### -----
###
### * The PSD will be interpolated with linear interpolation if needed.
###   The user is highly encouraged to plot the result to see if it is satisfactory.
###
###
### Review status
### -------------
###
### +-----------------------------------------+------------------------------------------+------------+
### | Reviewers                               | Hash                                     | Date       |
### +=========================================+==========================================+============+
### | Florent, Duncan Me., Jolien, Kipp, Chad | e5b22699b049f7a35dff468febad471ca6c737f3 | 2014-04-29 |
### +-----------------------------------------+------------------------------------------+------------+
###

parser = OptionParser(description = __doc__)
parser.add_option("--instrument", help="instrument, e.g. H1")
parser.add_option("--output", metavar = "filename", help = "Set the name of the LIGO light-weight XML file to output")
parser.add_option("--df", metavar = "float", type = "float", default = 0.25, help = "set the frequency resolution to interpolate to, default = 0.25")
parser.add_option("--type", metavar = "type", default = "ASD", help = "input is ASD or PSD, default ASD")

options, filenames = parser.parse_args()

if options.type == "ASD":
	read_as_psd = False
elif options.type == "PSD":
	read_as_psd = True
else:
	raise ValueError("--type must be ASD or PSD")

#FIXME hack to pad the series since it doesn't start at 0
psd = read_asd_txt(filenames[0], df=options.df, zero_pad=True, read_as_psd=read_as_psd)

write_psd(options.output, {options.instrument: psd})
