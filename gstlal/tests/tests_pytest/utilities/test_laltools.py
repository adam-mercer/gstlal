"""Unit tests for laltools module

"""
import pathlib
import tempfile

from lal.utils import CacheEntry

from gstlal.utilities import laltools

DATA_ROOT = pathlib.Path(__file__).parent.parent.parent / 'data'
FILE_1 = DATA_ROOT / 'L-L1_GWOSC_O3a_4KHZ_R1-1238437888-4096.gwf'

entries = [
	FILE_1.as_posix(),
	FILE_1,
	CacheEntry.from_T050017(FILE_1.as_uri()),
]


class TestCache:
	"""Tests for LAL cache creation"""

	def test_cache(self):
		"""Test cache"""

		with tempfile.TemporaryDirectory() as tmpdir:
			cache_path = pathlib.Path(tmpdir) / 'tmp.cache'

			res_path = laltools.create_cache(entries, cache_path)

			assert res_path.exists()
