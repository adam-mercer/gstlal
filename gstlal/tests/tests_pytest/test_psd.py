"""Unit tests for psd module

"""

from numpy.testing import assert_allclose

import lal

from gstlal import psd
from gstlal.utilities.testtools import GstLALTestManager


def test_interpolate_psd(reference_psd):
	"""Test interpolate_psd.
	"""
	interp_df = 1. / 128
	interp_psd = psd.interpolate_psd(reference_psd, interp_df)

	assert isinstance(interp_psd, lal.REAL8FrequencySeries)
	assert interp_psd.f0 == reference_psd.f0
	assert interp_psd.deltaF == interp_df


def test_movingmedian(reference_psd):
	"""Test movingmedian on psd.
	"""
	median_psd = psd.movingmedian(reference_psd, window_size=15)

	assert isinstance(median_psd, lal.REAL8FrequencySeries)
	assert median_psd.f0 == reference_psd.f0
	assert median_psd.deltaF == reference_psd.deltaF


def test_read_write_psd(reference_psd):
	"""Test read_psd and write_psd in a round trip.
	"""
	with GstLALTestManager() as gsttm:
		filename = (gsttm.tmp_path / "psd.xml.gz").as_posix()
		ifo = "H1"
		psd_dict = {ifo: reference_psd}
		psd.write_psd(filename, psd_dict)
		this_psd = psd.read_psd(filename)[ifo]

		assert isinstance(this_psd, lal.REAL8FrequencySeries)

		assert this_psd.name == reference_psd.name
		assert this_psd.epoch == reference_psd.epoch
		assert this_psd.f0 == reference_psd.f0
		assert this_psd.deltaF == reference_psd.deltaF
		assert this_psd.sampleUnits == reference_psd.sampleUnits

		assert_allclose(this_psd.data.data, reference_psd.data.data, rtol=1e-10)
