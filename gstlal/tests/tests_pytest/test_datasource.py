"""Unittests for DataSources

"""
import optparse
import pathlib

import gi
import pytest

from gstlal.utilities import testtools

gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst

GObject.threads_init()
Gst.init(None)

from gstlal import datasource
from gstlal.datasource import DataSource, Detector, DEFAULT_STATE_CHANNEL, DEFAULT_DQ_CHANNEL
from gstlal.utilities.testtools import GstLALTestManager, skip_osx, clean_str


# Comment out test of possible alternate copyright mechanism
#
# class TestCopyright:
#     """Test copyright information properly set"""
#
#     def test_copyright(self):
#         """Test copyright"""
#         exp = """Copyright (C) 2009--2013 Kipp Cannon, Chad Hanna, Drew Keppel
#         This program is free software; you can redistribute it and/or modify it
#         under the terms of the GNU General Public License as published by the
#         Free Software Foundation; either version 2 of the License, or (at your
#         option) any later version.
#
#         This program is distributed in the hope that it will be useful, but
#         WITHOUT ANY WARRANTY; without even the implied warranty of
#         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
#         Public License for more details.
#
#         You should have received a copy of the GNU General Public License along
#         with this program; if not, write to the Free Software Foundation, Inc.,
#         51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA."""
#         assert clean_str(datasource.__copyright__) == clean_str(exp)


class TestConstants:
	"""Test class wrapper for module constants"""

	def test_datasource_enum(self):
		"""Tests for datasource enum behavior"""
		assert DataSource.ALIGO == 'AdvLIGO'
		assert DataSource.AVirgo == 'AdvVirgo'
		assert DataSource.Frames == 'frames'
		assert DataSource.FrameXMIT == 'framexmit'
		assert DataSource.LIGO == 'LIGO'
		assert DataSource.LVSHM == 'lvshm'
		assert DataSource.NDS == 'nds'
		assert DataSource.Silence == 'silence'
		assert DataSource.White == 'white'

		# Test .value attributes
		assert DataSource.LIGO.value == 'LIGO'

	def test_detector_enum(self):
		"""Tests for detector enum"""
		assert Detector.G1 == 'G1'
		assert Detector.H1 == 'H1'
		assert Detector.H2 == 'H2'
		assert Detector.K1 == 'K1'
		assert Detector.L1 == 'L1'
		assert Detector.V1 == 'V1'

		# Test .value attributes
		assert Detector.H1.value == 'H1'

	def test_default_channels_dicts(self):
		"""Test default channel dicts"""
		assert 'H1' in DEFAULT_STATE_CHANNEL
		assert 'H1' in DEFAULT_DQ_CHANNEL

	def test_misc_constants(self):
		"""Tests to freeze values of constants"""
		assert 'frames' in datasource.KNOWN_DATASOURCES


class TestParsing:
	"""Test class wrapper for datasource parsing utilities"""

	def test_parse_list_to_dict(self):
		"""Test parsing a list to a dict"""
		# Test simple case
		res = datasource.parse_list_to_dict(["H1=LSC-STRAIN", "H2=SOMETHING-ELSE"])
		exp = {'H1': 'LSC-STRAIN', 'H2': 'SOMETHING-ELSE'}
		assert res == exp

		# test case of range-keys
		res = datasource.parse_list_to_dict(["0000:0002:H1=LSC_STRAIN_1,L1=LSC_STRAIN_2", "0002:0004:H1=LSC_STRAIN_3,L1=LSC_STRAIN_4", "0004:0006:H1=LSC_STRAIN_5,L1=LSC_STRAIN_6"],
											key_is_range=True)
		exp = {'0000': {'H1': 'LSC_STRAIN_1', 'L1': 'LSC_STRAIN_2'}, '0001': {'H1': 'LSC_STRAIN_1', 'L1': 'LSC_STRAIN_2'}, '0002': {'H1': 'LSC_STRAIN_3', 'L1': 'LSC_STRAIN_4'},
			   '0003': {'H1': 'LSC_STRAIN_3', 'L1': 'LSC_STRAIN_4'}, '0004': {'H1': 'LSC_STRAIN_5', 'L1': 'LSC_STRAIN_6'}, '0005': {'H1': 'LSC_STRAIN_5', 'L1': 'LSC_STRAIN_6'}}
		assert res == exp

		# verify transparent to None
		assert datasource.parse_list_to_dict(None) is None

		# verify can replace frame_type_dict_*
		res = datasource.parse_list_to_dict(['H1=H1_GWOSC_O2_16KHZ_R1', 'L1=L1_GWOSC_O2_16KHZ_R1'])
		exp = {'H1': 'H1_GWOSC_O2_16KHZ_R1', 'L1': 'L1_GWOSC_O2_16KHZ_R1'}
		assert res == exp

		# verify can replace framexmit_dict_*
		res = datasource.parse_list_to_dict(["H1=224.3.2.1:7096", "L1=224.3.2.2:7097", "V1=224.3.2.3:7098"], value_transform=datasource.parse_host)
		exp = {'H1': ('224.3.2.1', 7096), 'L1': ('224.3.2.2', 7097), 'V1': ('224.3.2.3', 7098)}
		assert res == exp

		# verify can replace injection_dict_*
		res = datasource.parse_list_to_dict(["0000:0002:Injection_1.xml", "0002:0004:Injection_2.xml"], key_is_range=True)
		exp = {'0000': 'Injection_1.xml', '0001': 'Injection_1.xml', '0002': 'Injection_2.xml', '0003': 'Injection_2.xml'}
		assert res == exp

		# verify can replace channel_dict*
		res = datasource.parse_list_to_dict(["0000:0002:H1=LSC_STRAIN_1,L1=LSC_STRAIN_2", "0002:0004:H1=LSC_STRAIN_3,L1=LSC_STRAIN_4", "0004:0006:H1=LSC_STRAIN_5,L1=LSC_STRAIN_6"],
											key_is_range=True)
		exp = {'0000': {'H1': 'LSC_STRAIN_1', 'L1': 'LSC_STRAIN_2'}, '0001': {'H1': 'LSC_STRAIN_1', 'L1': 'LSC_STRAIN_2'}, '0002': {'H1': 'LSC_STRAIN_3', 'L1': 'LSC_STRAIN_4'},
			   '0003': {'H1': 'LSC_STRAIN_3', 'L1': 'LSC_STRAIN_4'}, '0004': {'H1': 'LSC_STRAIN_5', 'L1': 'LSC_STRAIN_6'}, '0005': {'H1': 'LSC_STRAIN_5', 'L1': 'LSC_STRAIN_6'}}
		assert res == exp

	def test_ravel_dict_to_list(self):
		"""Tests for parse_dict_to_list"""

		# Verify can replace framexmit_list_*
		res = datasource.ravel_dict_to_list({'V1': ('224.3.2.3', 7098), 'H1': ('224.3.2.1', 7096), 'L1': ('224.3.2.2', 7097)},
											value_transform=datasource.ravel_host, join_arg='framexmit-addr')
		exp = 'H1=224.3.2.1:7096 --framexmit-addr=L1=224.3.2.2:7097 --framexmit-addr=V1=224.3.2.3:7098'
		assert res == exp

		# verify can replace state_vector_on_off_list_*
		res = datasource.ravel_dict_to_list({"H1": [0x7, 0x160], "H2": [0x7, 0x160], "L1": [0x7, 0x160], "V1": [0x67, 0x100]}, unzip=True,
											join_arg=['state-vector-on-bits', 'state-vector-off-bits'])
		exp = ('H1=7 --state-vector-on-bits=H2=7 --state-vector-on-bits=L1=7 --state-vector-on-bits=V1=103',
			   'H1=352 --state-vector-off-bits=H2=352 --state-vector-off-bits=L1=352 --state-vector-off-bits=V1=256')
		assert tuple(res) == exp

		# verify can replace pipeline_channel_list_*
		res = datasource.ravel_dict_to_list({'H2': 'SOMETHING-ELSE', 'H1': 'LSC-STRAIN'}, join_arg='channel-name')
		exp = 'H1=LSC-STRAIN --channel-name=H2=SOMETHING-ELSE'
		assert res == exp

		res = datasource.ravel_dict_to_list({'H2': 'SOMETHING-ELSE', 'H1': 'LSC-STRAIN'}, filter_keys=["H1"], join_arg='channel-name')
		exp = 'H1=LSC-STRAIN'
		assert res == exp

		# verify can replace pipelint_channel_list_*_node_range
		res = datasource.ravel_dict_to_list({'0000': {'H2': 'SOMETHING-ELSE', 'H1': 'LSC-STRAIN'}}, key_is_range=True, range_elem=0, join_arg='channel-name')
		exp = 'H1=LSC-STRAIN --channel-name=H2=SOMETHING-ELSE'
		assert res == exp

	def test_zip_dict(self):
		"""Test for zip dict util"""
		on = off = {}
		default = {'H1': [7, 352], 'H2': [7, 352], 'L1': [7, 352], 'V1': [103, 256]}
		res = datasource.zip_dict_values(on, off, defaults=default)
		base = {'H1': [7, 352], 'H2': [7, 352], 'L1': [7, 352], 'V1': [103, 256]}
		assert res == base

		# verify can replace state_vector_on_off_dict_*
		on_bit_dict = datasource.parse_list_to_dict(["V1=7", "H1=7", "L1=7"], value_transform=int)
		off_bit_dict = datasource.parse_list_to_dict(["V1=256", "H1=352", "L1=352"], value_transform=int)
		res = datasource.zip_dict_values(on_bit_dict, off_bit_dict, defaults=datasource.DEFAULT_STATE_VECTOR_ON_OFF)
		exp = {'H1': [7, 352], 'H2': [7, 352], 'L1': [7, 352], 'V1': [7, 256]}
		assert res == exp


class TestLegacyParsing:
	"""Test class wrapper for legacy datasource parsing utilities"""

	def test_state_vector_on_off_dict_from_bit_lists(self):
		on_bit_list = ["V1=7", "H1=7", "L1=7"]
		off_bit_list = ["V1=256", "H1=352", "L1=352"]
		res = datasource.state_vector_on_off_dict_from_bit_lists(on_bit_list, off_bit_list)
		base = {'H1': [7, 352], 'H2': [7, 352], 'L1': [7, 352], 'V1': [7, 256]}
		assert res == base

		res = datasource.state_vector_on_off_dict_from_bit_lists(on_bit_list, off_bit_list, {})
		base = {'V1': [7, 256], 'H1': [7, 352], 'L1': [7, 352]}
		assert res == base

		on_bit_list = ["V1=0x7", "H1=0x7", "L1=0x7"]
		off_bit_list = ["V1=0x256", "H1=0x352", "L1=0x352"]
		res = datasource.state_vector_on_off_dict_from_bit_lists(on_bit_list, off_bit_list, {})
		base = {'V1': [7, 598], 'H1': [7, 850], 'L1': [7, 850]}
		assert res == base


class TestDataSourceInfoInit:
	"""Test class for new DataSourceInfo api"""

	def test_init_aligo(self):
		"""Test creating a DataSourceInfo in pure python, datasource: advligo"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.ALIGO,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
		assert isinstance(info, datasource.DataSourceInfo)

	def test_init_avirgo(self):
		"""Test creating a DataSourceInfo in pure python, datasource: advvirgo"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.AVirgo,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
		assert isinstance(info, datasource.DataSourceInfo)

	def test_init_frames(self):
		"""Test creating a DataSourceInfo in pure python, datasource: frames"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.Frames,
											 frame_cache=gsttm.cache_path,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
		assert isinstance(info, datasource.DataSourceInfo)

	def test_init_framexmit(self):
		"""Test creating a DataSourceInfo in pure python, datasource: framexmit"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.FrameXMIT,
											 channel_name={Detector.H1: 'CHANNEL'})
		assert isinstance(info, datasource.DataSourceInfo)

	def test_init_ligo(self):
		"""Test creating a DataSourceInfo in pure python, datasource: ligo"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.LIGO,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
		assert isinstance(info, datasource.DataSourceInfo)

	def test_init_lvshm(self):
		"""Test creating a DataSourceInfo in pure python, datasource: lvshm"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.LVSHM,
											 channel_name={Detector.H1: 'CHANNEL'})
		assert isinstance(info, datasource.DataSourceInfo)

	def test_init_nds(self):
		"""Test creating a DataSourceInfo in pure python, datasource: nds"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.NDS,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'},
											 nds_host='HOST',
											 nds_port=1234)
		assert isinstance(info, datasource.DataSourceInfo)

	def test_init_silence(self):
		"""Test creating a DataSourceInfo in pure python, datasource: silence"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.Silence,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
		assert isinstance(info, datasource.DataSourceInfo)

	def test_init_white(self):
		"""Test creating a DataSourceInfo in pure python, datasource: white"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.White,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
		assert isinstance(info, datasource.DataSourceInfo)

	def test_persist_frame_cache_fileobj(self):
		"""Test creating a DataSourceInfo in pure python, datasource: frames"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.Frames,
											 frame_cache=None,
											 frame_type={'H1': 'H1_GWOSC_O2_16KHZ_R1'},
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
			cache_path = pathlib.Path(info._frame_cache_fileobj.name)
			assert cache_path.exists()


def assert_info_mkbasicsrc(info, detector):
	"""Utility for testing an Info object can be made into a basic source pipeline"""
	mainloop = GObject.MainLoop()
	pipeline = Gst.Pipeline("test_mkbasicsrc")

	head, *_ = datasource.mkbasicsrc(pipeline, info, detector, verbose=False)
	assert type(head).__name__ == 'GstAudioConvert'


class TestDataSourceInfoBasicSource:
	"""Test class for new DataSourceInfo api"""

	@pytest.mark.skip('Fake sources not currently working with Gst 1.0 api')
	def test_mkbasicsrc_aligo(self):
		"""Test that new api works with mkbasicsrc, datasource: AdvLIGO"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.ALIGO,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
			assert_info_mkbasicsrc(info, Detector.H1.value)

	@pytest.mark.skip('Fake sources not currently working with Gst 1.0 api')
	def test_mkbasicsrc_avirgo(self):
		"""Test that new api works with mkbasicsrc, datasource: AdvVirgo"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.AVirgo,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
			assert_info_mkbasicsrc(info, Detector.H1.value)

	@pytest.mark.skip('Fake sources not currently working with Gst 1.0 api')
	def test_mkbasicsrc_ligo(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.LIGO,
											 channel_name={Detector.H1: 'CHANNEL'},
											 gps_start_time=1234567,
											 gps_end_time=1234568)
			assert_info_mkbasicsrc(info, Detector.H1.value)

	def test_mkbasicsrc_silence(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.Silence,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
			assert_info_mkbasicsrc(info, Detector.H1.value)

	def test_mkbasicsrc_white(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.White,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
			assert_info_mkbasicsrc(info, Detector.H1.value)


@testtools.requires_full_build
class TestDataSourceInfoBasicSourceUgly:
	"""Test class for new DataSourceInfo api, these
	tests require gstlal-ugly to be build first so they are grouped separately
	and marked accordingly.
	"""

	def test_mkbasicsrc_frames(self):
		"""Test that new api works with mkbasicsrc, datasource: frames"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.Frames,
											 frame_cache=gsttm.cache_path,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'})
			assert_info_mkbasicsrc(info, Detector.H1.value)

	@skip_osx  # problem with gds conda packages on osx with gstlal-ugly
	def test_mkbasicsrc_framexmit(self):
		"""Test that new api works with mkbasicsrc, datasource: framexmit"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.FrameXMIT,
											 channel_name={Detector.H1: 'CHANNEL'})
			assert_info_mkbasicsrc(info, Detector.H1.value)

	@skip_osx  # problem with gds conda packages on osx with gstlal-ugly
	def test_mkbasicsrc_lvshm(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.LVSHM,
											 channel_name={Detector.H1: 'CHANNEL'})
			assert_info_mkbasicsrc(info, Detector.H1.value)

	def test_mkbasicsrc_nds(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			info = datasource.DataSourceInfo(data_source=DataSource.NDS,
											 gps_start_time=1234567,
											 gps_end_time=1234568,
											 channel_name={Detector.H1: 'CHANNEL'},
											 nds_host='HOST',
											 nds_port=1234)
			assert_info_mkbasicsrc(info, Detector.H1.value)


class TestDataSourceInfoBasicSourceOptParse:
	"""Test class wrapper for testing new datasource api optparse integration
	These tests are the specific drop-in replacements for the old api usage in scripts
	"""

	@testtools.broken('Fake sources not currently working with Gst 1.0 api')
	def test_mkbasicsrc_aligo(self):
		"""Test that new api works with mkbasicsrc, datasource: AdvLIGO"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=AdvLIGO',
				'--channel-name=H1=CHANNEL',
				'--gps-start-time=1234567',
				'--gps-end-time=1234568',
			]
			parser = optparse.OptionParser(description="test legacy mkbasicsrc")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)

	@testtools.broken('Fake sources not currently working with Gst 1.0 api')
	def test_mkbasicsrc_avirgo(self):
		"""Test that new api works with mkbasicsrc, datasource: AdvVirgo"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=AdvVirgo',
				'--channel-name=H1=CHANNEL',
				'--gps-start-time=1234567',
				'--gps-end-time=1234568',
			]
			parser = optparse.OptionParser(description="test legacy mkbasicsrc")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)

	@testtools.broken('Fake sources not currently working with Gst 1.0 api')
	def test_mkbasicsrc_ligo(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=LIGO',
				'--channel-name=H1=CHANNEL',
				'--gps-start-time=1234567',
				'--gps-end-time=1234568',
			]
			parser = optparse.OptionParser(description="test legacy mkbasicsrc")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)

	def test_mkbasicsrc_silence(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=silence',
				'--channel-name=H1=CHANNEL',
				'--gps-start-time=1234567',
				'--gps-end-time=1234568',
			]
			parser = optparse.OptionParser(description="test legacy mkbasicsrc")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)

	def test_mkbasicsrc_white(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=white',
				'--channel-name=H1=CHANNEL',
				'--gps-start-time=1234567',
				'--gps-end-time=1234568',
			]
			parser = optparse.OptionParser(description="test legacy mkbasicsrc")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)


@testtools.requires_full_build
class TestDataSourceInfoBasicSourceOptParseUgly:
	"""Test class wrapper for testing new datasource api optparse integration
	These tests are the specific drop-in replacements for the old api usage in scripts

	Separate group for tests requiring gstlal-ugly gst plugins
	"""

	def test_mkbasicsrc_frames(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=frames',
				'--frame-cache={}'.format(gsttm.cache_path),
				'--channel-name=H1=CHANNEL',
				'--gps-start-time=1234567',
				'--gps-end-time=1234568',
			]
			parser = optparse.OptionParser(description="test legacy mkbasicsrc")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)

	@skip_osx  # problem with gds conda packages on osx with gstlal-ugly
	def test_mkbasicsrc_framexmit(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=framexmit',
				'--channel-name=H1=CHANNEL',
			]
			parser = optparse.OptionParser(description="test legacy mkbasicsrc")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)

	@skip_osx  # problem with gds conda packages on osx with gstlal-ugly
	def test_mkbasicsrc_lvshm(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=lvshm',
				'--channel-name=H1=CHANNEL',
			]
			parser = optparse.OptionParser(description="test legacy mkbasicsrc")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)

	def test_mkbasicsrc_nds(self):
		"""Test that new api works with mkbasicsrc"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=nds',
				'--channel-name=H1=CHANNEL',
				'--gps-start-time=1234567',
				'--gps-end-time=1234568',
				'--nds-host=HOST',
				'--nds-port=1234',
			]
			parser = optparse.OptionParser(description="test legacy mkbasicsrc")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)

	def test_on_off_bits_coercion(self):
		"""Test that on off bits are being coerced"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=lvshm',
				'--channel-name=H1=CHANNEL',
				'--state-vector-on-bits=H1=3',
				'--state-vector-on-bits=L1=3',
				'--state-vector-on-bits=V1=1027',
				'--state-vector-off-bits=H1=0',
				'--state-vector-off-bits=L1=0',
				'--state-vector-off-bits=V1=0'
			]
			parser = optparse.OptionParser(description="test legacy on off coercion")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)
			for k in ('H1', 'H2', 'V1', 'L1'):
				for v in info.state_vector_on_off_bits[k]:
					assert isinstance(v, int)

	def test_state_vector_channel(self):
		"""Test that state vector channel is being updated"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=lvshm',
				'--channel-name=H1=CHANNEL',
				'--state-channel-name=H1=STATE',
			]
			parser = optparse.OptionParser(description="test state vector channel")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)
			assert info.state_channel_name[Detector.H1.value] == 'STATE'

	def test_dq_vector_channel(self):
		"""Test that dq vector channel is being updated"""
		with GstLALTestManager() as gsttm:
			args = [
				'--data-source=lvshm',
				'--channel-name=H1=CHANNEL',
				'--dq-channel-name=H1=DQVECTOR',
			]
			parser = optparse.OptionParser(description="test dq vector channel")
			datasource.append_options(parser)
			options, filenames = parser.parse_args(args)

			info = datasource.DataSourceInfo.from_optparse(options)
			assert_info_mkbasicsrc(info, Detector.H1.value)
			assert info.dq_channel_name[Detector.H1.value] == 'DQVECTOR'
