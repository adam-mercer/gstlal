"""Unittests for filter pipeparts"""
import numpy
import pytest

from gstlal import gstpipetools
from gstlal.pipeparts import filters, pipetools
from gstlal.pipeparts.pipetools import Gst, GObject


def assert_elem_props(elem, name):
	assert gstpipetools.is_element(elem)
	assert type(elem).__name__ == name


class TestFilters:
	"""Test filter pipeparts"""

	@pytest.fixture(scope='class', autouse=True)
	def pipeline(self):
		GObject.MainLoop()
		return Gst.Pipeline('TestEncode')

	@pytest.fixture(scope='class', autouse=True)
	def src(self, pipeline):
		return pipetools.make_element_with_src(pipeline, None, 'fakesrc')

	def test_caps(self, pipeline, src):
		"""Test caps filter"""
		elem = filters.caps(pipeline, src, caps="audio/x-raw, rate=1")
		assert_elem_props(elem, "GstCapsFilter")

	def test_fir(self, pipeline, src):
		"""Test create a FIR filter"""
		elem = filters.fir(pipeline, src, kernel=None, latency=None)
		assert_elem_props(elem, "GstAudioFIRFilter")

	def test_iir(self, pipeline, src):
		"""Test create IIR filter"""
		elem = filters.iir(pipeline, src, a=numpy.array([1, 2, 3]), b=numpy.array([1, 2, 3]))
		assert_elem_props(elem, "GstAudioIIRFilter")

	def test_state_vector(self, pipeline, src):
		"""Test state_vector"""
		elem = filters.state_vector(pipeline, src)
		assert_elem_props(elem, "GSTLALStateVector")

	def test_inject(self, pipeline, src):
		"""Test injections"""
		elem = filters.inject(pipeline, src, 'file')
		assert_elem_props(elem, "GSTLALSimulation")

	def test_audio_cheb_band(self, pipeline, src):
		"""Test cheb"""
		elem = filters.audio_cheb_band(pipeline, src, 10.0, 100.0)
		assert_elem_props(elem, "GstAudioChebBand")

	def test_audio_cheb_limit(self, pipeline, src):
		"""Test cheb limit"""
		elem = filters.audio_cheb_limit(pipeline, src, 10.0)
		assert_elem_props(elem, "GstAudioChebLimit")

	def test_drop(self, pipeline, src):
		"""Test drop"""
		elem = filters.drop(pipeline, src, 10)
		assert_elem_props(elem, "GSTLALDrop")

	def test_remove_fake_disconts(self, pipeline, src):
		"""Test removefd"""
		elem = filters.remove_fake_disconts(pipeline, src)
		assert_elem_props(elem, "GSTLALNoFakeDisconts")

	def test_gate(self, pipeline, src):
		"""Test gate"""
		elem = filters.gate(pipeline, src, control=src)
		assert_elem_props(elem, "GSTLALGate")
