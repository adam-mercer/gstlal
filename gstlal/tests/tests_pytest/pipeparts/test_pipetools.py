"""Unit tests for pipetools module"""
import inspect

from gstlal import gstpipetools
from gstlal.pipeparts import pipetools
from gstlal.utilities import testtools


# Will reuse a million times:
"""

		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		src:
			Gst.Element, the source element
		**properties: 
			dict, keyword arguments to be set as element properties

"""


class TestPipetools:
	"""Test class group for pipetools"""

	def test_clean_property_name(self):
		"""Test cleaning of property names"""
		assert pipetools.clean_property_name('name_with_underscores') == 'name-with-underscores'
		assert pipetools.clean_property_name('async_') == 'async'

	def test_make_element_with_src(self):
		"""Test make element"""
		with testtools.GstLALTestManager(with_pipeline=True) as tm:
			elem = pipetools.make_element_with_src(tm.pipeline, None, 'fakesrc')
			assert gstpipetools.is_element(elem)

