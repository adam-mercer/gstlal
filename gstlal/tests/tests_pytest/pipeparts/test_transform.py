"""Unittests for transform pipeparts"""
import pytest
from lal import LIGOTimeGPS

from gstlal import gstpipetools
from gstlal.pipeparts import pipetools, transform
from gstlal.pipeparts.pipetools import Gst
from gstlal.utilities import testtools


def assert_elem_props(elem, name):
	assert gstpipetools.is_element(elem)
	assert type(elem).__name__ == name


class TestTransform:
	"""Group test for transform elements"""

	@pytest.fixture(scope='class', autouse=True)
	def pipeline(self):
		return Gst.Pipeline('TestEncode')

	@pytest.fixture(scope='class', autouse=True)
	def src(self, pipeline):
		return pipetools.make_element_with_src(pipeline, None, 'fakesrc')

	def test_sum_squares(self, pipeline, src):
		"""Test sumsquares"""
		elem = transform.sum_squares(pipeline, src)
		assert_elem_props(elem, 'GSTLALSumSquares')

	def test_tag_inject(self, pipeline, src):
		"""Test sumsquares"""
		elem = transform.tag_inject(pipeline, src, 'a,b,c')
		assert_elem_props(elem, 'GstTagInject')

	def test_shift(self, pipeline, src):
		"""Test shift"""
		elem = transform.shift(pipeline, src)
		assert_elem_props(elem, 'GSTLALShift')

	def test_audio_amplify(self, pipeline, src):
		"""Test audio amplify"""
		elem = transform.amplify(pipeline, src, 0.1)
		assert_elem_props(elem, 'GstAudioAmplify')

	def test_audio_undersample(self, pipeline, src):
		"""Test audio amplify"""
		elem = transform.undersample(pipeline, src)
		assert_elem_props(elem, 'GSTLALAudioUnderSample')

	def test_audio_resample(self, pipeline, src):
		"""Test audio resample"""
		elem = transform.resample(pipeline, src)
		assert_elem_props(elem, 'GstAudioResample')

	def test_whiten(self, pipeline, src):
		"""Test whiten"""
		elem = transform.whiten(pipeline, src, "GSTLAL_PSDMODE_FIXED")
		assert_elem_props(elem, 'GSTLALWhiten')

	def test_tee(self, pipeline, src):
		"""Test tee"""
		elem = transform.tee(pipeline, src)
		assert_elem_props(elem, 'GstTee')

	def test_adder(self, pipeline, src):
		"""Test adder"""
		elem = transform.adder(pipeline, [src, src])
		assert_elem_props(elem, 'GstLALAdder')

	def test_multiplier(self, pipeline, src):
		"""Test adder"""
		elem = transform.adder(pipeline, [src, src])
		assert_elem_props(elem, 'GstLALAdder')

	def test_queue(self, pipeline, src):
		"""Test queue"""
		elem = transform.queue(pipeline, src)
		assert_elem_props(elem, 'GstQueue')

	def test_fir_bank(self, pipeline, src):
		"""Test fir_bank"""
		elem = transform.fir_bank(pipeline, src)
		assert_elem_props(elem, 'GSTLALFIRBank')

	def test_reblock(self, pipeline, src):
		"""Test reblock"""
		elem = transform.reblock(pipeline, src)
		assert_elem_props(elem, 'GSTLALReblock')

	def test_matrixmixer(self, pipeline, src):
		"""Test matrixmixer"""
		elem = transform.matrix_mixer(pipeline, src)
		assert_elem_props(elem, 'GSTLALMatrixMixer')

	def test_toggle_complex(self, pipeline, src):
		"""Test matrixmixer"""
		elem = transform.toggle_complex(pipeline, src)
		assert_elem_props(elem, 'GSTLALToggleComplex')

	def test_auto_chisq(self, pipeline, src):
		"""Test """
		elem = transform.auto_chisq(pipeline, src)
		assert_elem_props(elem, 'GSTLALAutoChiSq')

	def test_colorspace(self, pipeline, src):
		"""Test """
		elem = transform.colorspace(pipeline, src)
		assert_elem_props(elem, 'GstVideoConvert')

	def test_audioconvert(self, pipeline, src):
		"""Test """
		elem = transform.audio_convert(pipeline, src)
		assert_elem_props(elem, 'GstAudioConvert')

	def test_audiorate(self, pipeline, src):
		"""Test """
		elem = transform.audio_rate(pipeline, src)
		assert_elem_props(elem, 'GstAudioRate')

	def test_peak(self, pipeline, src):
		"""Test """
		elem = transform.peak(pipeline, src, 10)
		assert_elem_props(elem, 'GSTLALPeak')

	def test_set_caps(self, pipeline, src):
		"""Test caps setter"""
		elem = transform.set_caps(pipeline, src, caps="audio/x-raw, rate=1")
		assert_elem_props(elem, "GstCapsSetter")

	def test_progress_report(self, pipeline, src):
		"""Test progress report"""
		elem = transform.progress_report(pipeline, src, 'name')
		assert_elem_props(elem, 'GstProgressReport')


@testtools.requires_full_build
class TestTransformFullBuild:
	"""Group test for transform elements"""

	@pytest.fixture(scope='class', autouse=True)
	def pipeline(self):
		return Gst.Pipeline('TestTransformFullBuild')

	@pytest.fixture(scope='class', autouse=True)
	def src(self, pipeline):
		return pipetools.make_element_with_src(pipeline, None, 'fakesrc')

	def test_bitvectorgen(self, pipeline, src):
		"""Test bvg"""
		elem = transform.bit_vector_gen(pipeline, src, 1)
		assert_elem_props(elem, 'GSTLALBitVectorGen')

	def test_deglitch(self, pipeline, src):
		"""Test """
		elem = transform.deglitch(pipeline, src, [(LIGOTimeGPS(123456), LIGOTimeGPS(123457))])
		assert_elem_props(elem, 'GstLALDeglitchFilter')

	def test_denoise(self, pipeline, src):
		"""Test """
		elem = transform.denoise(pipeline, src)
		assert_elem_props(elem, 'GSTLALDenoiser')

	def test_clean(self, pipeline, src):
		"""Test """
		elem = transform.clean(pipeline, src)
		assert_elem_props(elem, 'GSTLALDenoiser')

	def test_latency(self, pipeline, src):
		"""Test """
		elem = transform.latency(pipeline, src)
		assert_elem_props(elem, 'GSTLALLatency')


class TestTransformBroken:
	"""Broken tests"""

	@testtools.broken('Causes seg fault')
	def test_td_whiten(self, pipeline, src):
		"""Test fir_bank"""
		elem = transform.td_whiten(pipeline, src)
		assert_elem_props(elem, 'GSTLALTDwhiten')

	@testtools.broken('Unable to find abs')
	def test_abs(self, pipeline, src):
		"""Test abs"""
		elem = transform.abs_(pipeline, src)
		assert_elem_props(elem, 'GstMultiFileSink')

	@testtools.broken('Unable to find pow')
	def test_pow(self, pipeline, src):
		"""Test pow"""
		elem = transform.pow(pipeline, src)
		assert_elem_props(elem, 'GstMultiFileSink')

	@testtools.broken('Crashed test suite on mac')
	def test_check_timestamps(self, pipeline, src):
		"""Test """
		elem = transform.check_timestamps(pipeline, src)
		assert_elem_props(elem, 'GstLALDeglitchFilter')

	@testtools.impl_deprecated
	@testtools.requires_full_build
	def test_mean(self, pipeline, src):
		"""Test mean"""
		elem = transform.mean(pipeline, src)
		assert_elem_props(elem, 'GstMultiFileSink')

	@testtools.broken('Python plugins are broken')
	@testtools.requires_full_build
	def test_coherentnull(self, pipeline, src):
		"""Test """
		elem = transform.lho_coherent_null(pipeline, src, src, None, None, None, None, 1)
		assert_elem_props(elem, 'GSTLALDenoiser')

	@testtools.broken('Crashed test suite on mac')
	@testtools.requires_full_build
	def test_check_timestamps(self, pipeline, src):
		"""Test """
		elem = transform.check_timestamps(pipeline, src)
		assert_elem_props(elem, 'GstLALDeglitchFilter')

	@testtools.impl_deprecated
	@testtools.requires_full_build
	def test_trim(self, pipeline, src):
		"""Test trim"""
		elem = transform.trim(pipeline, src)
		assert_elem_props(elem, 'GSTLALTrim')

	@testtools.broken('Causes seg fault')
	@testtools.requires_full_build
	def test_interpolator(self, pipeline, src):
		"""Test interpolator"""
		elem = transform.interpolator(pipeline, src)
		assert_elem_props(elem, 'GSTLALInterpolator')
