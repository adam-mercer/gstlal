#!/usr/bin/env python3
#
# Copyright (C) 2021 Soichiro Kuwahara
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""Cherenkov burst searching tool"""

#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#


import numpy
from optparse import OptionParser
import sys
import tempfile
import threading
import gi
gi.require_version('Gst','1.0')
from gi.repository import GObject
GObject.threads_init()
from gi.repository import Gst
Gst.init(None)

import lal
from lal import series as lalseries
from lalburst import snglcoinc
import lalsimulation

from gstlal import datasource
from gstlal import pipeio
from gstlal import pipeparts
from gstlal.pipeparts import condition
from gstlal import simplehandler
from gstlal import snglbursttable
from gstlal import streamburca
from gstlal import cherenkov
from gstlal.cherenkov import rankingstat as cherenkov_rankingstat

from ligo import segments
from ligo.lw import ligolw
from ligo.lw import lsctables
from ligo.lw import utils as ligolw_utils
from ligo.lw.utils import ligolw_add
from ligo.lw.utils import process as ligolw_process
from ligo.lw.utils import segments as ligolw_segments
from ligo.lw.utils import time_slide as ligolw_time_slide


pipeparts.mkchecktimestamps = lambda pipeline, src, *args: src


@lsctables.use_in
class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
	pass


program_name = "gstlal_cherenkov_burst"


#
# ================================================================================
#
#                            Custom SnglBurst Type
#
# ================================================================================
#


#
# override .template_id to induce the desired exact-match template
# comparison in the coincidence code
#


class SnglBurst(snglbursttable.GSTLALSnglBurst):
	@property
	def template_id(self):
		return (self.central_freq, self.bandwidth)


#
# ================================================================================
#
#                                  Command Line
#
# ================================================================================
#


def parse_command_line():
	# prepare parser
	parser = OptionParser(
		description = "GstLAL-based cherenkov burst search pipeline."
	)
	parser.add_option("--verbose", action = "store_true", help = "Be verbose.")
	parser.add_option("--template-bank", metavar = "filename", help = "Name of template file. Template bank for all detectors involved must be given.")
	parser.add_option("--time-slide-file", metavar = "filename", help = "Name of time slide file.  Required.")
	parser.add_option("--reference-psd", metavar = "filename", help = "Use fixed PSDs from this file (one for each detector) instead of measuring them from the data.  Optional.")
	parser.add_option("--output", metavar = "filename", help = "Name of output xml file.")
	parser.add_option("--rankingstat-output", metavar = "filename", help = "Name of output xml file to record rankingstat object.")
	parser.add_option("--threshold", metavar = "snr_threshold", type = "float", default = 4.0, help = "SNR threshold (default = 4.0).")
	parser.add_option("--cluster-events", metavar = "cluster_events", type = "float", default = 0.1, help = "Cluster events with input timescale in seconds (default = 0.1).")
	parser.add_option("--min-instruments", metavar = "min_instruments", type = "int", default = 2, help = "Minimum number of instruments (default is 2).")
	parser.add_option("--snr-dump", metavar = "start,stop", help = "Turn on SNR time series dumping to a text file within the GPS time range [start, stop).")
	parser.add_option("--hoft-dump", metavar = "start,stop", help = "Turn on hoft dumping to a text file within the GPS time range [start, stop).")
	datasource.append_options(parser)

	# parse command line
	options, filenames = parser.parse_args()

	# do this before modifying the options object
	process_params = options.__dict__.copy()

	# check for missing options
	required_options = set(("template_bank", "time_slide_file", "output", "rankingstat_output"))
	missing_options = set(option for option in required_options if getattr(options, option) is None)
	if missing_options:
		raise ValueError("missing required option(s) %s" % ", ".join("--%s" % option.subst("_", "-") for option in (required_options - missing_options)))

	options.gps_start_time = lal.LIGOTimeGPS(options.gps_start_time)
	options.gps_end_time = lal.LIGOTimeGPS(options.gps_end_time)

	datasourceinfo = datasource.DataSourceInfo.from_optparse(options)

	if options.snr_dump is not None:
		options.snr_dump = segments.segment(*map(lal.LIGOTimeGPS, options.snr_dump.split(",")))

	if options.hoft_dump is not None:
		options.hoft_dump = segments.segment(*map(lal.LIGOTimeGPS, options.hoft_dump.split(",")))
	return options, process_params, datasourceinfo, filenames


#
# ================================================================================
#
#                               Pipeline Handler
#
# ================================================================================
#


class PipelineHandler(simplehandler.Handler):
	def __init__(self, mainloop, pipeline, firbank, xmldoc_output, process, template_bank, triggergen, rankingstat, is_noninjections):
		super(PipelineHandler, self).__init__(mainloop, pipeline)
		self.lock = threading.Lock()
		self.firbank = firbank
		self.sngl_burst = lsctables.SnglBurstTable.get_table(xmldoc_output)
		self.process = process
		self.template_bank = template_bank
		self.triggergen = triggergen
		self.update_psd = dict.fromkeys(firbank, 0)
		self.rankingstat = rankingstat
		self.is_noninjections = is_noninjections
		self.streamburca = streamburca.StreamBurca(xmldoc_output, process.process_id, rankingstat.delta_t, cherenkov.CherenkovBBCoincDef, min_instruments = rankingstat.min_instruments)


	def appsink_new_buffer(self, elem):
		with self.lock:
			buf = elem.emit("pull-sample").get_buffer()
			events = []
			for i in range(buf.n_memory()):
				memory = buf.peek_memory(i)
				result, mapinfo = memory.map(Gst.MapFlags.READ)
				assert result
				if mapinfo.data:
					events.extend(SnglBurst.from_buffer(mapinfo.data))
				memory.unmap(mapinfo)

			# get ifo from the appsink name property
			instrument = elem.get_property("name")

			# set event process id and event id
			for event in events:
				event.process_id = self.process.process_id
				event.event_id = self.sngl_burst.get_next_id()
				assert event.ifo == instrument

			# extract segment. move the segment's upper
			# boundary to include all triggers.
			buf_timestamp = lal.LIGOTimeGPS(0, buf.pts)
			buf_seg = segments.segment(buf_timestamp, max(buf_timestamp + lal.LIGOTimeGPS(0, buf.duration), max(event.peak for event in events) if events else 0.0))
			if buf.mini_object.flags & Gst.BufferFlags.GAP:
				# sanity check that gap buffers are empty
				assert not events
			else:
				# update trigger rate and livetime
				self.rankingstat.denominator.triggerrates[instrument].add_ratebin(tuple(map(float, buf_seg)), len(events))

			# push the single detector trigger into the
			# StreamBurca instance. the push method returns
			# True if the coincidence engine has new results.
			# In that case, call the pull() method to run the
			# coincidence engine.
			if self.streamburca.push(instrument, events, buf_timestamp):
				self.streamburca.pull(self.rankingstat, self.rankingstat.segmentlists, noninjections = self.is_noninjections)

	def flush(self):
		self.streamburca.pull(self.rankingstat, self.rankingstat.segmentlists, noninjections = self.is_noninjections, flush = True)

	def down_sample(self, psd, deltaf):
		width = deltaf / psd.deltaF
		# width must be integer otherwise we cant down sample psd.
		assert width == int(width)

		psd_data = psd.data.data
		psd_data = [numpy.mean(psd_data[int(width * i):int(width * (i+1))], dtype = numpy.float64) for i in range(int(len(psd_data - 1) / width))]
		psd_data = numpy.append(psd_data, 0.0)

		new_psd = lal.CreateREAL8FrequencySeries("new_psd", psd.epoch, psd.f0, deltaf, psd.sampleUnits, len(psd_data))
		new_psd.data.data = psd_data

		return new_psd

	def make_templates(self, instrument, psd):
		# Create matrices to add firbank.
		template_t = [None] * len(self.template_bank)
		autocorr = [None] * len(self.template_bank)

		# FIXME: down-sample PSD to, say, \Delta f = 0.5 Hz
		psd = self.down_sample(psd, 0.5)

		# obtain the value of sample rate and PSD kernel duration
		sample_rate = 2 * (psd.f0 + psd.deltaF * (psd.data.length - 1))
		assert sample_rate == int(sample_rate)
		sample_rate = int(sample_rate)
		fir_duration = 1. / psd.deltaF
		assert fir_duration == int(fir_duration)
		fir_length = int(fir_duration) * sample_rate

		# This is to set rplan and fplan here for avoiding calculate these again in for loop.
		# NOTE:This can be done because the length of the templates is fixed to fir_length.
		fplan = lal.CreateForwardREAL8FFTPlan(fir_length, 0)
		rplan = lal.CreateReverseREAL8FFTPlan(fir_length, 0)
		template_f = lal.CreateCOMPLEX16FrequencySeries("template_freq", psd.epoch, 0.0, psd.deltaF, lal.Unit("strain s"), fir_length // 2 + 1)
		template_f_squared = lal.CreateCOMPLEX16FrequencySeries("template_freq_squared", psd.epoch, 0.0, psd.deltaF, lal.Unit("strain s"), fir_length // 2 + 1)
		autocorr_t = lal.CreateREAL8TimeSeries("autocorr", psd.epoch, 0.0, 1.0 / sample_rate, lal.Unit("strain"), fir_length)
		# FIXME:  adjust beta = what fraction of the window is in the tapers
		window = lal.CreateTukeyREAL8Window(fir_length, 0.5).data.data

		# make templates, whiten, put into firbank
		for i, row in enumerate(self.template_bank):
			# obtain cherenkov radiation-like waveform. Since this wave form is linealy polarized, only the plus mode will be considered.
			template_t[i], _ = lalsimulation.SimBurstCherenkovRadiation(row.central_freq, row.bandwidth, row.amplitude, 1.0 / sample_rate)

			# Resize the template as its deltaF matches to the PSD.
			assert template_t[i].data.length <= fir_length, "data length is too huge comparted to the zero pad."
			lal.ResizeREAL8TimeSeries(template_t[i], -(fir_length - template_t[i].data.length) // 2, fir_length)

			# Save the template file before whiten NOTE: Comment out when the waveform looks okay.
			if False:
				taxis = numpy.arange(template_t[i].data.length, dtype = "double") * template_t[i].deltaT + float(template_t[i].epoch)
				template = template_t[i].data.data
				template = numpy.array(list(map(list, zip(taxis,template))))

			# FFT to frequency domain
			lal.REAL8TimeFreqFFT(template_f, template_t[i], fplan)

			# set DC and Nyquist to zero
			template_f.data.data[0] = 0.0
			template_f.data.data[template_f.data.length-1] = 0.0

			# Save the template in frequency domain to check workability. NOTE: Comment out when this mission is completed.
			if False:
				spectrum = numpy.zeros(template_f.data.length)
				data_spectrum = numpy.zeros(psd.data.length)
				for j in range(template_f.data.length):
					f = template_f.f0 + template_f.deltaF * j
					spectrum[j] = f * f * (template_f.data.data[j].real * template_f.data.data[j].real + template_f.data.data[j].imag * template_f.data.data[j].imag)
				numpy.savetxt("spectrum_beforewhiten.txt", spectrum, delimiter = ",")
				numpy.savetxt("sample_template_faxis.txt", numpy.arange(template_f.data.length, dtype = "double") * template_f.deltaF + template_f.f0, delimiter = ",")

			# whiten
			lal.WhitenCOMPLEX16FrequencySeries(template_f, psd)

			# Save the template in frequecncy domain after whiten to check. NOTE: Comment out when this mission is completed.
			if False:
				for j in range(template_f.data.length):
					f = template_f.f0 + template_f.deltaF * j
					spectrum[j] = f * f * (template_f.data.data[j].real * template_f.data.data[j].real + template_f.data.data[j].imag * template_f.data.data[j].imag)
					data_spectrum[j] = numpy.sqrt(psd.data.data[j])
				numpy.savetxt("spectrum_afterwhiten.txt", spectrum, delimiter = ",")
				numpy.savetxt("data_spectrum.txt", data_spectrum, delimiter = ",")

			# Perform inverse fft and get the template in time series which is whitened
			lal.REAL8FreqTimeFFT(template_t[i], template_f, rplan)

			# Obtain autocorrelation time series and add a sample of 0 in the tail to make the number of samle odd
			template_f_squared.data.data = abs(template_f.data.data)**2
			lal.REAL8FreqTimeFFT(autocorr_t, template_f_squared, rplan)

			# Set the length of autocorrelation matrix to 2 seconds because we know this is lonh enough.
			autocorr[i] = autocorr_t.data.data
			autocorr[i] = numpy.concatenate((autocorr[i][1:(int(1 * sample_rate) + 1)][::-1], autocorr[i][:(int(1 * sample_rate) + 1)]))
			assert len(autocorr[i]) % 2 == 1, "autocorr must have odd number of samples."

			# normalize autocorrelation by central value so that central value becomes 1
			autocorr[i] /= autocorr[i].max()

			# apply Tukey window and normalize
			template_t[i] = template_t[i].data.data * window
			template_t[i] /= numpy.sqrt(numpy.dot(template_t[i], template_t[i]))
			if False:
				template = numpy.insert(template, 2, template_t[i], axis =1)
				numpy.savetxt("sample_template.txt", template, delimiter= " ")

			# make the number of samples odd by adding a sample of 0 in the tail
			template_t[i] = numpy.append(template_t[i], 0.0)
			assert len(template_t[i]) % 2 == 1, 'template must have odd number of samples.'
			if False:
				numpy.savetxt("autocornew.txt", autocorr[i], delimiter = ",")
				#numpy.savetxt("sample_template_afterwhiten.txt", template_t[i], delimiter = ",")

		# set them to the filter bank
		self.firbank[instrument].set_property("fir_matrix", template_t)
		self.triggergen[instrument].set_property("autocorrelation_matrix", autocorr)

	def do_on_message(self, bus, message):
		if message.type == Gst.MessageType.ELEMENT and message.get_structure().get_name() == "spectrum":
			instrument = message.src.get_name().split("_")[-1]
			psd = pipeio.parse_spectrum_message(message)

			# To get proper PSD, make a condition on stability.
			stability = float(message.src.get_property("n-samples")) / message.src.get_property("average-samples")
			if stability > 0.3:
				if self.update_psd[instrument] != 0:
					self.update_psd[instrument] -=1
				else:
					self.update_psd[instrument] = 20
					# FIXME:  do this only once if using a reference psd
					print("At GPS time", psd.epoch, "setting whitened templates for", instrument, file=sys.stderr)
					self.make_templates(instrument, psd)
			else:
				# Use templates with all zeros so that we won't get any triggers.
				if options.verbose:
					print("At GPS time", psd.epoch, "burn in period", file = sys.stderr)
				sample_rate = 2 * (psd.f0 + psd.deltaF * (psd.data.length - 1))
				self.firbank[instrument].set_property("fir_matrix", [numpy.zeros(int(2 * sample_rate + 1))] * len(self.template_bank))
				self.triggergen[instrument].set_property("autocorrelation_matrix", [numpy.zeros(int(2 * sample_rate + 1))] * len(self.template_bank))
			return True
		return False


#
# ================================================================================
#
#                                     Main
#
# ================================================================================
#


#
# parse command line
#


options, process_params, datasourceinfo, filenames = parse_command_line()

# Obtain the instruments

all_inst = tuple(datasourceinfo.channel_dict.keys())


#
# load template bank file
#


template_bank_xml = ligolw_utils.load_filename(options.template_bank, contenthandler = LIGOLWContentHandler, verbose = options.verbose)
template_bank = lsctables.SnglBurstTable.get_table(template_bank_xml)


#
# load optional reference PSD
#


if options.reference_psd:
	reference_psds = lalseries.read_psd_xmldoc(ligolw_utils.load_filename(options.reference_psd, contenthandler = lalseries.PSDContentHandler, verbose = options.verbose))
else:
	reference_psds = None


#
# load time slide file and optionally injection file.  this will be used as
# the starting point for our output file (lazy).  check that he offset
# vectors match our instrument list
#


xmldoc_output = ligolw_add.ligolw_add(ligolw.Document(), [options.time_slide_file] + ([options.injections] if options.injections else []), verbose = options.verbose)
for time_slide_id, offset_vector in lsctables.TimeSlideTable.get_table(xmldoc_output).as_dict().items():
	if set(offset_vector) != set(all_inst):
		raise ValueError("time slide ID %d has wrong instruments:  %s, need %s" % (time_slide_id, ", ".join("%s" % instrument for instrument in sorted(offset_vector)), ", ".join("%s" % instrument for instrument in sorted(all_inst))))


#
# initialize output
#


process = ligolw_process.register_to_xmldoc(xmldoc_output, program_name, process_params)
sngl_burst_table = xmldoc_output.childNodes[-1].appendChild(lsctables.New(lsctables.SnglBurstTable, ("process:process_id", "ifo", "central_freq", "bandwidth", "snr", "chisq", "chisq_dof", "peak_time", "peak_time_ns", "event_id")))
sngl_burst_table.sync_next_id()


#
# initialize ranking statistic
#


rankingstat = cherenkov_rankingstat.RankingStat(all_inst, delta_t = 0.010, min_instruments = options.min_instruments)


#
# built graph
#


mainloop = GObject.MainLoop()
pipeline = Gst.Pipeline(name = "pipeline")

firbank = dict.fromkeys(all_inst, None)
triggergen = dict.fromkeys(all_inst, None)

for instrument in all_inst:
	# for frame file, statevector and dqvector are None
	head, statevector, dqvector = datasource.mkbasicsrc(pipeline, datasourceinfo, instrument, options.verbose)
	#
	# dump hoft series.
	#

	if options.hoft_dump is not None:
		head = pipeparts.mktee(pipeline, head)
		pipeparts.mknxydumpsink(pipeline, head, "%s_hoft_dump_%s.txt" % (program_name, instrument), segment = options.hoft_dump)

	rate = 2048
	head = condition.mkcondition(
		pipeline,
		src = head,
		target_rate = rate,
		instrument = instrument,
		width = 32,
		statevector = statevector,
		dqvector = dqvector,
		psd = reference_psds[instrument] if reference_psds is not None else None,
		psd_fft_length = 32,
		ht_gate_threshold = float("inf"),
		track_psd = reference_psds is None
	)


	#
	# dump whitened hoft series.
	#


	if options.hoft_dump is not None:
		head = pipeparts.mktee(pipeline, head)
		pipeparts.mknxydumpsink(pipeline, head, "%s_hoft_whiten_dump_%s_%d.txt" % (program_name, instrument, rate), segment = options.hoft_dump)


	#
	# filter bank
	#


	head = firbank[instrument] = pipeparts.mkfirbank(pipeline, head, fir_matrix = numpy.zeros((len(template_bank), int(2 * rate) + 1), dtype = numpy.float64), block_stride = 4 * rate, latency = int(rate))


	#
	# dump snr time series. Use for impulse resoponse checking.
	#


	if options.snr_dump is not None:
		head = pipeparts.mktee(pipeline, head)
		pipeparts.mknxydumpsink(pipeline, head, "%s_snr_dump_%s_%d.txt" % (program_name, instrument, rate), segment = options.snr_dump)


	#
	# trigger generator
	# Using temp file adjusting the "ifo" column of template_bank to instrument in for loop.
	#


	with tempfile.NamedTemporaryFile(suffix = ".xml.gz") as fp:
		template_bank.getColumnByName("ifo")[:] = [instrument for i in range(len(template_bank))]
		xmldoc = ligolw.Document()
		xmldoc.appendChild(ligolw.LIGO_LW())
		xmldoc.childNodes[-1].appendChild(template_bank)
		ligolw_utils.write_filename(xmldoc, fp.name, verbose = options.verbose)
		head = triggergen[instrument] = pipeparts.mkgeneric(pipeline, head, "lal_string_triggergen", threshold = options.threshold, cluster = options.cluster_events, bank_filename = fp.name, autocorrelation_matrix = numpy.zeros((len(template_bank), int(2 * rate + 1)), dtype = numpy.float64))


#
# handler
#


handler = PipelineHandler(mainloop, pipeline, firbank, xmldoc_output, process, template_bank, triggergen, rankingstat, is_noninjections = options.injections is None)


#
# appsync
#


appsync = pipeparts.AppSync(appsink_new_buffer = handler.appsink_new_buffer)
appsinks = set(appsync.add_sink(pipeline, triggergen[inst], caps = Gst.Caps.from_string("application/x-lal-snglburst"), name = inst) for inst in all_inst)


#
# seek
#


if pipeline.set_state(Gst.State.READY) != Gst.StateChangeReturn.SUCCESS:
	raise RuntimeError("pipeline did not enter ready state")
datasource.pipeline_seek_for_gps(pipeline, options.gps_start_time, options.gps_end_time)


#
# run
#


if pipeline.set_state(Gst.State.PLAYING) != Gst.StateChangeReturn.SUCCESS:
	raise RuntimeError("pipeline did not enter playing state")
try:
	pipeparts.write_dump_dot(pipeline, "%s.%s" % ("cherenkov-burst", "NULL"), verbose = options.verbose)
except ValueError:
	pass
if options.verbose:
	print("running pipeline ...", file = sys.stderr)
mainloop.run()

handler.flush()


#
# write output
#


ligolw_utils.write_filename(xmldoc_output, options.output, verbose = options.verbose)

# write rankingstat object to an XML file
xmldoc_rankingstat = ligolw.Document()
xmldoc_rankingstat.appendChild(ligolw.LIGO_LW())
process_rankingstat = ligolw_process.register_to_xmldoc(xmldoc_rankingstat, program_name, process_params)
xmldoc_rankingstat.childNodes[-1].appendChild(rankingstat.to_xml("rankingstat"))
ligolw_utils.write_filename(xmldoc_rankingstat, options.rankingstat_output, verbose = options.verbose)
